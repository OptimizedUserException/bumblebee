## Open Source Policy

### License
MIT

### Checklist before moving to Open Source // TODO
- [ ] Add separate remote that is Open Source only.
- [ ] Split `AndroidManifest.xml` into multiple files to remove Fabric Key.
- [ ] Add `fabric.properties` file to .gitignore