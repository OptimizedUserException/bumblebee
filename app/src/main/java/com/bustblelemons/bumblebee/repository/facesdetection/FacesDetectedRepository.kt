package com.bustblelemons.bumblebee.repository.facesdetection

import com.bustblelemons.bumblebee.ui.camera.FacesDetection
import com.bustblelemons.bumblebee.ui.camera.OnFacesDetected
import com.bustblelemons.bumblebee.ui.camera.di.CameraScope
import io.fotoapparat.Fotoapparat
import io.fotoapparat.FotoapparatBuilder
import io.fotoapparat.facedetector.Rectangle
import io.fotoapparat.view.CameraRenderer
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

@CameraScope class FacesDetectedRepository @Inject constructor(
        private val facesDetectedSubject: BehaviorSubject<Array<Rectangle>>,
        private val pressureStrategy: BackpressureStrategy = BackpressureStrategy.LATEST,
        private val facesDetection: FacesDetection,
        private val fotoapparatBuilder: FotoapparatBuilder
) : OnFacesDetected {

    init {
        facesDetection.facesDetected = this
    }

    private lateinit var fotoAparat: Fotoapparat

    override fun onFacesDetected(faces: List<Rectangle>) {
        if (faces.isEmpty()) return
        facesDetectedSubject.onNext(faces.toTypedArray())
    }

    val detectFaces: Flowable<Array<Rectangle>>
        get() = facesDetectedSubject.toFlowable(pressureStrategy)

    fun start(cameraRenderer: CameraRenderer) {
        facesDetection.facesDetected = this
        fotoAparat = fotoapparatBuilder.into(cameraRenderer).build()
        fotoAparat.start()
    }

    fun stop() {
        fotoAparat.stop()
        facesDetection.facesDetected = FacesDetection.EMPTY_FACES_DETECTED
        facesDetectedSubject.onComplete()
    }
}