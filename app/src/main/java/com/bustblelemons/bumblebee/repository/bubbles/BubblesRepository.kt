package com.bustblelemons.bumblebee.repository.bubbles

import com.bustblelemons.bumblebee.ui.camera.di.CameraScope
import com.bustblelemons.bumblebee.ui.comicbubble.ComicBubble
import javax.inject.Inject

@CameraScope class BubblesRepository @Inject constructor() {

    private var knownBubbles: MutableList<ComicBubble> = mutableListOf()

    val bubbles: List<ComicBubble>
        get() = knownBubbles

    fun addBubble(bubble: ComicBubble) {
        knownBubbles.add(bubble)
    }

    fun removeBubble(bubble: ComicBubble) {
        knownBubbles.remove(bubble)
    }
}