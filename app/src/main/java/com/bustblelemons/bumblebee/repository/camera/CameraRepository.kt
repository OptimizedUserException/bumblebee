package com.bustblelemons.bumblebee.repository.camera

import android.graphics.Rect
import com.bustblelemons.bumblebee.ui.camera.di.CameraScope
import com.bustblelemons.bumblebee.ui.cameragrid.CameraGrid
import com.bustblelemons.bumblebee.ui.cameragrid.ThreeByThreeCameraGrid
import javax.inject.Inject

@CameraScope
class CameraRepository constructor(
        private var latestCameraBounds: Rect,
        private var latestCameraGrid: CameraGrid
) {

    @Inject
    constructor() : this(Rect(), ThreeByThreeCameraGrid(Rect()))

    val cameraGrid: CameraGrid
        get() = latestCameraGrid

    val cameraBounds: Rect
        get() = latestCameraBounds

    fun latestCameraBounds(cameraBounds: Rect) {
        latestCameraBounds = cameraBounds
    }

    fun latestCameraGrid(cameraGrid: CameraGrid) {
        this.latestCameraGrid = cameraGrid
    }
}