package com.bustblelemons.bumblebee.domain.model.functions.arrow

import android.graphics.Rect
import com.bustblelemons.bumblebee.domain.model.ArrowProperties
import com.bustblelemons.bumblebee.ui.comicbubble.BubbleArrow
import com.bustblelemons.bumblebee.ui.comicbubble.BubblePoints
import javax.inject.Inject

class BubbleArrowFromProperties @Inject constructor(
        private val towardCloseForProperties: ArrowTowardCloseForProperties,
        private val towardOpenForProperties: ArrowTowardOpenForProperties,
        private val arrowEndForProperties: ArrowEndForProperties,
        private val arrowPointForProperties: ArrowPointForAttachment,
        private val arrowStartForProperties: ArrowStartForProperties
) : (BubblePoints, ArrowProperties, Rect, Rect) -> BubbleArrow {

    override fun invoke(
            bubblePoints: BubblePoints,
            properties: ArrowProperties,
            totalBounds: Rect,
            face: Rect
    ): BubbleArrow {
        val start = arrowStartForProperties(properties, bubblePoints)
        val point = arrowPointForProperties(properties.attachment, totalBounds, face)
        val end = arrowEndForProperties(properties, bubblePoints)
        return BubbleArrow(
                start, point, end,
                towardOpenForProperties(properties, start, point),
                towardCloseForProperties(properties, end, point)
        )
    }
}