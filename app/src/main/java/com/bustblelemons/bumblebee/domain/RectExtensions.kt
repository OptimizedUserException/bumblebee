package com.bustblelemons.bumblebee.domain

import android.graphics.Rect
import com.bustblelemons.bumblebee.domain.model.Position

inline val Rect?.field: Int
    get() {
        if (this == null) return 0

        return this.width() * this.height()
    }

fun Rect.containsAtLeast(rect: Rect, percentage: Int): Boolean {
    if (!this.containsCornerOf(rect)) return false

    val fullField = this.field.toDouble()
    val requiredField: Double = fullField * (percentage.toDouble() / 100.0)

    val intersectingWidth = Math.abs(this.left - rect.right)
    val intersectingHeight = Math.abs(this.top - rect.bottom)
    val intersectingField = intersectingHeight * intersectingWidth

    return intersectingField >= requiredField.toInt()
}

inline fun Rect.containsCornerOf(rect: Rect): Boolean {
    if (this.contains(rect.top, rect.left)
            || this.contains(rect.top, rect.right)
            || this.contains(rect.bottom, rect.left)
            || this.contains(rect.bottom, rect.right)
    ) return true

    return false
}

inline fun Rect.containsCenterOf(rect: Rect): Boolean = this.contains(rect.centerX(), rect.centerY())


fun Rect.isWithinCenterXOf(rect: Rect, percentage: Float): Boolean {
    return this.centerX().isBetween(
            rect.centerX().minus(rect.width().times(percentage).toInt()),
            rect.centerX().plus(rect.width().times(percentage).toInt())
    )
}

fun Rect.isWithinCenterYOf(rect: Rect, percentage: Float): Boolean {
    return this.centerY().isBetween(
            rect.centerY().minus(rect.width().times(percentage).toInt()),
            rect.centerY().plus(rect.width().times(percentage).toInt())
    )
}

inline fun Int.isBetween(left: Int, right: Int): Boolean {
    return this in (left + 1)..(right - 1)
}


infix fun Rect.inPositionTo(rect: Rect): Position {
    return if (this isLeftOf rect) Position.Left
    else if (this isRightOf rect) Position.Right
    else if (this isAboveOf rect) Position.Top
    else Position.Bottom
}


infix fun Rect.isLeftOfCenterOf(rect: Rect): Boolean {
    return this.right < rect.centerX()
}

infix fun Rect.isLeftOf(rect: Rect): Boolean {
    return this.right <= rect.left
}

infix fun Rect.isRightOf(rect: Rect): Boolean {
    return this.left >= rect.right
}

infix fun Rect.isRightOfCenterOf(rect: Rect): Boolean {
    return this.left > rect.centerX()
}

infix fun Rect.isAboveOf(rect: Rect): Boolean {
    return this.bottom <= rect.top
}

infix fun Rect.isAboveCenterOf(rect: Rect): Boolean {
    return this.bottom <= rect.centerY()
}

infix fun Rect.isBelowCenterOf(rect: Rect): Boolean {
    return this.top >= rect.centerY()
}

