package com.bustblelemons.bumblebee.domain.usecase

import com.bustblelemons.bumblebee.domain.model.BubbleWithDirection
import com.bustblelemons.bumblebee.domain.model.functions.FaceRectsToDirectedComicBubbles
import com.bustblelemons.bumblebee.domain.model.functions.RectangleToRectMapper
import com.bustblelemons.bumblebee.repository.facesdetection.FacesDetectedRepository
import io.reactivex.Flowable
import javax.inject.Inject

class MapBubblesUseCase @Inject constructor(
        override val rxSchedulers: RxSchedulers,
        private val facesDetectedRepository: FacesDetectedRepository,
        private val rectangleToRectMapper: RectangleToRectMapper,
        private val faceRectsToComicBubbles: FaceRectsToDirectedComicBubbles
) : FlowableUseCase<Array<BubbleWithDirection>> {

    override val rxStream: Flowable<Array<BubbleWithDirection>>
        get() = facesDetectedRepository.detectFaces
                .map { rectangle -> rectangleToRectMapper(rectangle) }
                .map { faceRects -> faceRectsToComicBubbles(faceRects) }
                .map { it.toTypedArray() }
}
