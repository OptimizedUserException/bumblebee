package com.bustblelemons.bumblebee.domain.model.functions.arrow

import android.graphics.Point
import com.bustblelemons.bumblebee.domain.model.ArrowProperties
import com.bustblelemons.bumblebee.domain.model.Position
import com.bustblelemons.bumblebee.ui.comicbubble.algebra.CollinearPointBetween
import javax.inject.Inject

class ArrowTowardOpenForProperties @Inject constructor() : (ArrowProperties, Point, Point) -> Point {

    override fun invoke(properties: ArrowProperties, start: Point, point: Point): Point {
        return when (properties.attachment) {
            Position.Left -> leftTowards(properties, start, point)
            Position.Top -> topTowards(properties, start, point)
            Position.Right -> rightTowards(properties, start, point)
            Position.Bottom -> bottomTowards(properties, start, point)
            Position.None -> bottomTowards(properties, start, point)
        }
    }

    private fun leftTowards(properties: ArrowProperties, start: Point, point: Point): Point {
        return if (properties.pointsToward == Position.Top)
            CollinearPointBetween(
                    Point(point.x, start.y),
                    start
            )
        else if (properties.pointsToward == Position.Bottom)
            Point(point.x, start.y)
        else
            CollinearPointBetween(point, start)

    }

    private fun topTowards(properties: ArrowProperties, start: Point, point: Point): Point {
        return if (properties.pointsToward == Position.Left)
            CollinearPointBetween(
                    Point(start.x, point.y),
                    start
            )
        else if (properties.pointsToward == Position.Right)
            Point(start.x, point.y)
        else
            CollinearPointBetween(start, point)
    }

    private fun rightTowards(properties: ArrowProperties, start: Point, point: Point): Point {
        return if (properties.pointsToward == Position.Top)
            Point(point.x, start.y)
        else if (properties.pointsToward == Position.Bottom)
            CollinearPointBetween(
                    start,
                    Point(point.x, start.y)
            )
        else
            CollinearPointBetween(start, point)

    }

    private fun bottomTowards(properties: ArrowProperties, start: Point, point: Point): Point {
        return if (properties.pointsToward == Position.Left)
            CollinearPointBetween(
                    start,
                    Point(start.x, point.y)
            )
        else if (properties.pointsToward == Position.Right)
            Point(start.x, point.y)
        else
            CollinearPointBetween(start, point)
    }
}
