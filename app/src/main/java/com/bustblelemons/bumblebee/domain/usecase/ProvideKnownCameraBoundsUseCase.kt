package com.bustblelemons.bumblebee.domain.usecase

import android.graphics.Rect
import com.bustblelemons.bumblebee.repository.camera.CameraRepository
import io.reactivex.Completable
import javax.inject.Inject

class ProvideKnownCameraBoundsUseCase(
        override val rxSchedulers: RxSchedulers,
        private val cameraRepository: CameraRepository,
        private val cameraBounds: Rect?
) : CompletableUseCase {

    @Inject
    constructor(rxSchedulers: RxSchedulers, cameraRepository: CameraRepository)
            : this(rxSchedulers, cameraRepository, null)

    fun cameraBounds(cameraBounds: Rect): ProvideKnownCameraBoundsUseCase {
        return ProvideKnownCameraBoundsUseCase(this.rxSchedulers, this.cameraRepository, cameraBounds)
    }

    override val rxStream: Completable
        get() {
            if (cameraBounds == null) throw IllegalArgumentException("camera bounds has to be set")

            return Completable.fromCallable { cameraRepository.latestCameraBounds(cameraBounds) }
        }

}