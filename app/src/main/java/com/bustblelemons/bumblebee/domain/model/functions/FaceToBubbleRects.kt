package com.bustblelemons.bumblebee.domain.model.functions

import android.graphics.Rect
import com.bustblelemons.bumblebee.domain.containsCenterOf
import com.bustblelemons.bumblebee.repository.camera.CameraRepository
import com.bustblelemons.bumblebee.ui.cameragrid.ThreeByThreeCameraGrid
import javax.inject.Inject

interface FaceToBubbleRects : (Rect) -> Rect

class ThreeByThreeFaceToBubbleRects @Inject constructor(
        private val cameraGridProvider: ThreeByThreeCameraGridProvider
) : FaceToBubbleRects {

    private val mapping: Map<Rect, Rect> by lazy {
        return@lazy HashMap<Rect, Rect>().apply {
            cameraGridProvider.invoke().let { grid ->
                put(grid.topLeft, grid.bottomLeft)
                put(grid.topCenter, grid.topLeft)
                put(grid.topRight, grid.bottomRight)
                put(grid.centerLeft, grid.topRight)
                put(grid.center, grid.topLeft)
                put(grid.centerRight, grid.topLeft)
                put(grid.bottomLeft, grid.bottomRight)
                put(grid.bottomCenter, grid.topLeft)
                put(grid.bottomRight, grid.topRight)
            }
        }
    }

    override fun invoke(faceRect: Rect): Rect {
        return matchingFace(faceRect) ?: EMPTY_RECT
    }

    private fun matchingFace(faceRect: Rect): Rect? {
        return mapping
                .filterKeys { gridRect -> gridRect.containsCenterOf(faceRect) }
                .values
                .firstOrNull()
    }

    companion object {
        @JvmStatic private val EMPTY_RECT = Rect()
    }
}