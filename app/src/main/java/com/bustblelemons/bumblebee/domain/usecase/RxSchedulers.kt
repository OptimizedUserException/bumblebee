package com.bustblelemons.bumblebee.domain.usecase

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

interface RxSchedulers {
    val observeOn: Scheduler
    val subscribeOn: Scheduler
}

class ComputationSchedulers : RxSchedulers {
    override val observeOn: Scheduler get() = AndroidSchedulers.mainThread()
    override val subscribeOn: Scheduler get() = Schedulers.computation()
}