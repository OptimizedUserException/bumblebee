package com.bustblelemons.bumblebee.domain.model.functions

import android.graphics.Rect
import com.bustblelemons.bumblebee.domain.model.BubbleWithDirection
import com.bustblelemons.bumblebee.domain.model.functions.arrow.BubbleArrowFromProperties
import com.bustblelemons.bumblebee.domain.model.functions.arrow.CurvesForArrowProperties
import javax.inject.Inject

class FaceAndBubbleRectToDirectedBubble @Inject constructor(
        private val rectsToComicBubble: RectToComicBubble,
        private val arrowPropertiesForFace: ArrowPropertiesForFace,
        private val curvesForArrowProperties: CurvesForArrowProperties,
        private val bubblePointsFromProperties: BubblePointsFromProperties,
        private val bubbleArrowFromProperties: BubbleArrowFromProperties
) : (Rect, Rect) -> BubbleWithDirection {

    override fun invoke(face: Rect, bubble: Rect): BubbleWithDirection {
        val arrowProperties = arrowPropertiesForFace(face, bubble)
        val bubblePoints = bubblePointsFromProperties(arrowProperties, bubble)
        val bubbleArrow = bubbleArrowFromProperties(bubblePoints, arrowProperties, bubble, face)
        return BubbleWithDirection(rectsToComicBubble(bubble), face)
                .apply {
                    this.curves = curvesForArrowProperties(
                            arrowProperties.attachment,
                            bubblePoints,
                            bubbleArrow
                    )
                }
    }
}