package com.bustblelemons.bumblebee.domain.model

import android.graphics.Rect
import com.bustblelemons.bumblebee.ui.comicbubble.ComicBubble
import com.bustblelemons.bumblebee.ui.comicbubble.math.QuadraticBezierCurve

data class BubbleWithDirection(
        val bubble: ComicBubble,
        val face: Rect
) {

    var curves: Array<QuadraticBezierCurve>
        get() = bubble.comicBubblePath.curves
        set(value) {
            bubble.comicBubblePath.curves = value
        }

}