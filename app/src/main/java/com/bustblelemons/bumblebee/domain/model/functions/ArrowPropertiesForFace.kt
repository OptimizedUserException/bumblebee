package com.bustblelemons.bumblebee.domain.model.functions

import android.graphics.Rect
import com.bustblelemons.bumblebee.domain.inPositionTo
import com.bustblelemons.bumblebee.domain.model.ArrowProperties
import javax.inject.Inject

class ArrowPropertiesForFace @Inject constructor(
        private val pointsTowardPosition: PointsTowardPosition
) : (Rect, Rect) -> ArrowProperties {

    override fun invoke(face: Rect, bubble: Rect): ArrowProperties {
        return (face inPositionTo bubble).let { attachment ->
            ArrowProperties(
                    attachment,
                    pointsTowardPosition(attachment, face, bubble))
        }
    }
}
