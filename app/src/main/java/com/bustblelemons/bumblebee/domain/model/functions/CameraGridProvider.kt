package com.bustblelemons.bumblebee.domain.model.functions

import com.bustblelemons.bumblebee.repository.camera.CameraRepository
import com.bustblelemons.bumblebee.ui.cameragrid.CameraGrid
import com.bustblelemons.bumblebee.ui.cameragrid.ThreeByThreeCameraGrid
import javax.inject.Inject

interface CameraGridProvider<out Grid : CameraGrid> : () -> Grid

class ThreeByThreeCameraGridProvider @Inject constructor(
        private val cameraRepository: CameraRepository
) : CameraGridProvider<ThreeByThreeCameraGrid> {

    override fun invoke(): ThreeByThreeCameraGrid {
        return ThreeByThreeCameraGrid(cameraRepository.cameraBounds)
    }
}