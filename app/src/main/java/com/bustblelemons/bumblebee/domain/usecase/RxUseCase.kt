package com.bustblelemons.bumblebee.domain.usecase

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single

interface RxUseCase<RxType : Any> {
    val rxStream: RxType
}

interface RxScheduledUseCase<RxType : Any> {
    val rxScheduledStream: RxType
    val rxSchedulers: RxSchedulers
}

interface SingleUseCase<ObjectType> : RxUseCase<Single<ObjectType>>, RxScheduledUseCase<Single<ObjectType>> {
    override val rxScheduledStream: Single<ObjectType>
        get() = rxStream.observeOn(rxSchedulers.observeOn).subscribeOn(rxSchedulers.subscribeOn)
}

interface ObservableUseCase<ObjectType> : RxUseCase<Observable<ObjectType>>, RxScheduledUseCase<Observable<ObjectType>> {
    override val rxScheduledStream: Observable<ObjectType>
        get() = rxStream.observeOn(rxSchedulers.observeOn).subscribeOn(rxSchedulers.subscribeOn)
}

interface CompletableUseCase : RxUseCase<Completable>, RxScheduledUseCase<Completable> {
    override val rxScheduledStream: Completable
        get() = rxStream.observeOn(rxSchedulers.observeOn).subscribeOn(rxSchedulers.subscribeOn)
}

interface FlowableUseCase<ObjectType> :
        RxUseCase<Flowable<ObjectType>>, RxScheduledUseCase<Flowable<ObjectType>> {
    override val rxScheduledStream: Flowable<ObjectType>
        get() = rxStream.observeOn(rxSchedulers.observeOn).subscribeOn(rxSchedulers.subscribeOn)
}