package com.bustblelemons.bumblebee.domain.model

sealed class Position {
    object Left : Position()
    object Top : Position()
    object Right : Position()
    object Bottom : Position()
    object None : Position()
}