package com.bustblelemons.bumblebee.domain.model.functions

import android.graphics.Rect
import com.bustblelemons.bumblebee.domain.field
import javax.inject.Inject

class RectBySize @Inject constructor(/*empty*/) : Comparator<Rect> {

    override fun compare(left: Rect, right: Rect): Int = left.field.compareTo(right.field)
}