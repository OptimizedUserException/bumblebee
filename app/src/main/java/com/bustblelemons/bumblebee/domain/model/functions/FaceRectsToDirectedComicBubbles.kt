package com.bustblelemons.bumblebee.domain.model.functions

import android.graphics.Rect
import com.bustblelemons.bumblebee.domain.model.BubbleWithDirection
import com.bustblelemons.bumblebee.repository.bubbles.BubblesRepository
import javax.inject.Inject

class FaceRectsToDirectedComicBubbles @Inject constructor(
        private val rectSizeComparator: RectBySize,
        private val faceToBubbleRects: FaceToBubbleRects,
        private val faceAndBubbleRectToDirectedBubble: FaceAndBubbleRectToDirectedBubble,
        private val bubblesRepository: BubblesRepository
) : (List<Rect>) -> List<BubbleWithDirection> {

    override fun invoke(rects: List<Rect>): List<BubbleWithDirection> {
        val allFaces = rects.sortedWith(rectSizeComparator)
                .takeLast(FACE_THRESHOLD_LIMIT)
        val bubbles = bubblesRepository
                .bubbles
                .map { it.bounds }
                .take(allFaces.size)
        return allFaces
                .mapIndexed { index, face ->
                    if (index < bubbles.size) face to bubbles[index]
                    else face to faceToBubbleRects(face)
                }.map { (face, bubble) -> faceAndBubbleRectToDirectedBubble(face, bubble) }
    }

    companion object {
        const val FACE_THRESHOLD_LIMIT = 1
    }
}