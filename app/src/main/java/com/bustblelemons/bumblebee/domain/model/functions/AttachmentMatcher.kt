package com.bustblelemons.bumblebee.domain.model.functions

import android.graphics.Rect
import com.bustblelemons.bumblebee.domain.*
import com.bustblelemons.bumblebee.domain.model.Position
import com.bustblelemons.bumblebee.domain.model.functions.AttachmentMatcher.Companion.DEFAULT_CENTER_PERCENTAGE
import javax.inject.Inject

interface AttachmentMatcher : (Rect, Rect) -> Position {
    companion object {
        const val DEFAULT_CENTER_PERCENTAGE = 0.3F
    }
}

class LeftAttachmentMatcher @Inject constructor(/*empty*/) : HorizontalAttachmentMatcher() {
    override val centerPosition: Position = Position.Left
}

class RightAttachmentMatcher @Inject constructor(/*empty*/) : HorizontalAttachmentMatcher() {
    override val centerPosition: Position = Position.Right
}

abstract class HorizontalAttachmentMatcher constructor(
        private val centerPercentage: Float
) : AttachmentMatcher {

    constructor() : this(DEFAULT_CENTER_PERCENTAGE)

    abstract val centerPosition: Position

    override fun invoke(face: Rect, bubble: Rect): Position {
        return if (face.isWithinCenterYOf(bubble, centerPercentage)) centerPosition
        else if (face isAboveCenterOf bubble) Position.Top
        else if (face isBelowCenterOf bubble) Position.Bottom
        else Position.Bottom
    }
}

class TopAttachmentMatcher @Inject constructor(/*empty*/) : VerticalAttachmentMatcher() {
    override val centerPosition: Position = Position.Top
}

class BottomAttachmentMatcher @Inject constructor(/*empty*/) : VerticalAttachmentMatcher() {
    override val centerPosition: Position = Position.Bottom
}

abstract class VerticalAttachmentMatcher(
        private val centerPercentage: Float
) : AttachmentMatcher {

    constructor() : this(DEFAULT_CENTER_PERCENTAGE)

    abstract val centerPosition: Position
    
    override fun invoke(face: Rect, bubble: Rect): Position {
        return if (face.isWithinCenterXOf(bubble, centerPercentage)) centerPosition
        else if (face isLeftOfCenterOf bubble) Position.Left
        else if (face isRightOfCenterOf bubble) Position.Right
        else Position.Bottom
    }

}
