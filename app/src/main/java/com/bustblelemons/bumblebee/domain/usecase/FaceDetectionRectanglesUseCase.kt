package com.bustblelemons.bumblebee.domain.usecase

import com.bustblelemons.bumblebee.repository.facesdetection.FacesDetectedRepository
import io.fotoapparat.facedetector.Rectangle
import io.reactivex.Flowable
import javax.inject.Inject

class FaceDetectionRectanglesUseCase @Inject constructor(
        private val facesDetectedRepository: FacesDetectedRepository,
        override val rxSchedulers: RxSchedulers,
        private val rectangleComparator: RectangleComparator
) : FlowableUseCase<List<Rectangle>> {

    override val rxStream: Flowable<List<Rectangle>>
        get() = facesDetectedRepository
                .detectFaces
                .map { rectangles ->
                    rectangles.toList()
                            .sortedWith(rectangleComparator)
                            .takeLast(FACE_RECTANGLES_LIMIT)
                }

    companion object {
        const val FACE_RECTANGLES_LIMIT = 1
    }
}