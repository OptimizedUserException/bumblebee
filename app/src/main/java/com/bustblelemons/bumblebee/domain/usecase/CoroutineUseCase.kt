package com.bustblelemons.bumblebee.domain.usecase

interface CoroutineUseCase {
    companion object {
        const val LAUNCH_CONTEXT = "kotlin.coroutines.offthread"
    }
}