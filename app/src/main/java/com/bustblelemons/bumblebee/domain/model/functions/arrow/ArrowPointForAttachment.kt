package com.bustblelemons.bumblebee.domain.model.functions.arrow

import android.graphics.Point
import android.graphics.Rect
import com.bustblelemons.bumblebee.domain.model.IntersectionPoint
import com.bustblelemons.bumblebee.domain.model.Position
import javax.inject.Inject

class ArrowPointForAttachment @Inject constructor(/*empty*/) : (Position, Rect, Rect) -> Point {

    override fun invoke(attachment: Position, totalBounds: Rect, face: Rect): Point {
        val centerBubbleToCenterFace = totalBounds.centerPoint to face.centerPoint
        val bubbleIntersectionLine = intersectionLine(attachment, totalBounds)
        return IntersectionPoint(bubbleIntersectionLine, centerBubbleToCenterFace)
    }

    private fun intersectionLine(attachment: Position, totalBounds: Rect): Pair<Point, Point> {
        return when (attachment) {
            Position.Left -> totalBounds.leftLine
            Position.Top -> totalBounds.topLine
            Position.Right -> totalBounds.rightLine
            Position.Bottom -> totalBounds.bottomLine
            else -> totalBounds.bottomLine
        }
    }

    private val Rect.centerPoint: Point get() = Point(centerX(), centerY())

    private inline val Rect.leftLine: Pair<Point, Point> get() = topLeft to bottomLeft
    private inline val Rect.topLine: Pair<Point, Point> get() = topLeft to topRight
    private inline val Rect.rightLine: Pair<Point, Point> get() = topRight to bottomRight
    private inline val Rect.bottomLine: Pair<Point, Point> get() = bottomLeft to bottomRight

    private inline val Rect.bottomRight: Point get() = Point(this.right, this.bottom)
    private inline val Rect.bottomLeft: Point get() = Point(this.left, this.bottom)
    private inline val Rect.topLeft: Point get() = Point(this.left, this.top)
    private inline val Rect.topRight: Point get() = Point(this.right, this.top)

}
