package com.bustblelemons.bumblebee.domain.model.functions

import android.graphics.Rect
import com.bustblelemons.bumblebee.domain.model.Position
import javax.inject.Inject

class PointsTowardPosition @Inject constructor(
        private val leftAttachmentMatcher: LeftAttachmentMatcher,
        private val topAttachmentMatcher: TopAttachmentMatcher,
        private val rightAttachmentMatcher: RightAttachmentMatcher,
        private val bottomAttachmentMatcher: BottomAttachmentMatcher
) : (Position, Rect, Rect) -> Position {

    override fun invoke(attachment: Position, face: Rect, bubble: Rect): Position {
        return when (attachment) {
            Position.Left -> leftAttachmentMatcher(face, bubble)
            Position.Top -> topAttachmentMatcher(face, bubble)
            Position.Right -> rightAttachmentMatcher(face, bubble)
            Position.Bottom -> bottomAttachmentMatcher(face, bubble)
            Position.None -> invoke(Position.Bottom, face, bubble)
        }
    }
}