package com.bustblelemons.bumblebee.domain.usecase

import com.bustblelemons.bumblebee.repository.bubbles.BubblesRepository
import com.bustblelemons.bumblebee.ui.comicbubble.ComicBubble
import javax.inject.Inject

class PersistBubbleInMemoryUseCase @Inject constructor(
        private val bubblesRepository: BubblesRepository
) : (ComicBubble) -> Unit {

    override fun invoke(comicBubble: ComicBubble) = bubblesRepository.addBubble(comicBubble)
}