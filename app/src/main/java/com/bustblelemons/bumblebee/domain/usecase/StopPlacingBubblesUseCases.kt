package com.bustblelemons.bumblebee.domain.usecase

import com.bustblelemons.bumblebee.repository.facesdetection.FacesDetectedRepository
import io.reactivex.Completable
import javax.inject.Inject

class StopPlacingBubblesUseCases @Inject constructor(
        override val rxSchedulers: RxSchedulers,
        private val repository: FacesDetectedRepository
) : CompletableUseCase {

    override val rxStream: Completable
        get() = Completable.fromCallable {
            repository.stop()
        }
}
