package com.bustblelemons.bumblebee.domain.model.functions.arrow

import android.graphics.Point
import com.bustblelemons.bumblebee.domain.model.ArrowProperties
import com.bustblelemons.bumblebee.domain.model.Position
import com.bustblelemons.bumblebee.ui.comicbubble.BubblePoints
import com.bustblelemons.bumblebee.ui.comicbubble.algebra.CollinearPointBetween
import javax.inject.Inject

class ArrowEndForProperties @Inject constructor(/*empty*/) : (ArrowProperties, BubblePoints) -> Point {

    override fun invoke(properties: ArrowProperties, bubblePoints: BubblePoints): Point {
        return when (properties.attachment) {
            Position.Left -> leftEndPosition(bubblePoints, properties.arrowOpenSizePart)
            Position.Top -> topEndPosition(bubblePoints, properties.arrowOpenSizePart)
            Position.Right -> rightEndPosition(bubblePoints, properties.arrowOpenSizePart)
            Position.Bottom -> bottomEndPosition(bubblePoints, properties.arrowOpenSizePart)
            else -> bottomEndPosition(bubblePoints, properties.arrowOpenSizePart)
        }
    }

    private fun leftEndPosition(
            bubblePoints: BubblePoints,
            arrowOpenSizePart: Float
    ): Point {
        return CollinearPointBetween(
                bubblePoints.centerWest, bubblePoints.bottomWest,
                arrowOpenSizePart
        )
    }

    private fun topEndPosition(
            bubblePoints: BubblePoints,
            arrowOpenSizePart: Float
    ): Point {
        return CollinearPointBetween(
                bubblePoints.centerTop, bubblePoints.topEast,
                arrowOpenSizePart
        )
    }

    private fun bottomEndPosition(
            bubblePoints: BubblePoints,
            arrowOpenSizePart: Float
    ): Point {
        return CollinearPointBetween(
                bubblePoints.centerBottom, bubblePoints.bottomEast,
                arrowOpenSizePart
        )
    }

    private fun rightEndPosition(bubblePoints: BubblePoints, arrowOpenSizePart: Float): Point {
        return CollinearPointBetween(
                bubblePoints.centerEast, bubblePoints.bottomEast,
                arrowOpenSizePart
        )
    }
}
