package com.bustblelemons.bumblebee.domain.model.functions

import android.graphics.Rect
import com.bustblelemons.bumblebee.ui.comicbubble.BlackAndWhiteBubble
import com.bustblelemons.bumblebee.ui.comicbubble.ComicBubble
import javax.inject.Inject

class RectToComicBubble @Inject constructor(/*empty*/) : (Rect) -> ComicBubble {
    override fun invoke(bubbleBoundaries: Rect): ComicBubble {
        return BlackAndWhiteBubble().apply { bounds = bubbleBoundaries }
    }
}