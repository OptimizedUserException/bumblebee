package com.bustblelemons.bumblebee.domain.usecase

import com.bustblelemons.bumblebee.repository.bubbles.BubblesRepository
import com.bustblelemons.bumblebee.ui.comicbubble.ComicBubble
import javax.inject.Inject

class IsBubblePersistedInMemoryUseCase @Inject constructor(
        private val bubblesRepository: BubblesRepository
) : (ComicBubble) -> Boolean {

    override fun invoke(comicBubble: ComicBubble): Boolean {
        return bubblesRepository.bubbles.contains(comicBubble)
    }
}

