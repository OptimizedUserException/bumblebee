package com.bustblelemons.bumblebee.domain.usecase

import dagger.Module
import dagger.Provides

@Module
object UseCasesModule {

    @Provides
    @JvmStatic
    fun provideRxSchedulers(): RxSchedulers {
        return ComputationSchedulers()
    }
}