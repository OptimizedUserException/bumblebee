package com.bustblelemons.bumblebee.domain.model.functions

import android.graphics.Rect
import com.bustblelemons.bumblebee.domain.model.ArrowProperties
import com.bustblelemons.bumblebee.domain.model.Position
import com.bustblelemons.bumblebee.ui.comicbubble.BubblePoints
import javax.inject.Inject

class BubblePointsFromProperties @Inject constructor() : (ArrowProperties, Rect) -> BubblePoints {

    override fun invoke(arrowProperties: ArrowProperties, totalBounds: Rect): BubblePoints {
        return BubblePoints().apply {
            this.bounds = Rect(
                    left(totalBounds, arrowProperties),
                    top(totalBounds, arrowProperties),
                    right(totalBounds, arrowProperties),
                    bottom(totalBounds, arrowProperties)
            )
        }
    }

    private inline fun left(totalBounds: Rect, properties: ArrowProperties) =
            if (properties.attachment == Position.Left) properties.adjustedForAttachment(totalBounds)
            else totalBounds.left

    private inline fun bottom(totalBounds: Rect, properties: ArrowProperties) =
            if (properties.attachment == Position.Bottom) properties.adjustedForAttachment(totalBounds)
            else totalBounds.bottom

    private inline fun right(totalBounds: Rect, properties: ArrowProperties) =
            if (properties.attachment == Position.Right) properties.adjustedForAttachment(totalBounds)
            else totalBounds.right

    private inline fun top(totalBounds: Rect, properties: ArrowProperties) =
            if (properties.attachment == Position.Top) properties.adjustedForAttachment(totalBounds)
            else totalBounds.top

    private inline fun ArrowProperties.adjustedForAttachment(totalBounds: Rect): Int {
        return when (this.attachment) {
            Position.Left -> adjustPlus(totalBounds.left, totalBounds.width(), arrowSizePart)
            Position.Top -> adjustPlus(totalBounds.top, totalBounds.height(), arrowSizePart)
            Position.Right -> adjustMinus(totalBounds.right, totalBounds.width(), arrowSizePart)
            Position.Bottom -> adjustMinus(totalBounds.bottom, totalBounds.height(), arrowSizePart)
            else -> 0
        }
    }

    private val adjustPlus: (Int, Int, Float) -> Int = { sidePosition, sideSize, speechOpenPart ->
        sidePosition.plus(sideSize.times(speechOpenPart)).toInt()
    }

    private val adjustMinus: (Int, Int, Float) -> Int = { sidePosition, sideSize, speechOpenPart ->
        sidePosition.minus(sideSize.times(speechOpenPart)).toInt()
    }
}