package com.bustblelemons.bumblebee.domain.model.functions

import android.graphics.Rect
import com.bustblelemons.bumblebee.repository.camera.CameraRepository
import io.fotoapparat.facedetector.Rectangle
import javax.inject.Inject

class RectangleToRectMapper @Inject constructor(
        private val cameraRepository: CameraRepository
) : (Array<Rectangle>) -> List<Rect> {

    private val cameraWidth: Int get() = cameraRepository.cameraBounds.width()
    private val cameraHeight: Int get() = cameraRepository.cameraBounds.height()

    override fun invoke(rectangles: Array<Rectangle>): List<Rect> {
        return rectangles.map { rectangle ->
            val left = (rectangle.x * cameraWidth).toInt()
            val top = (rectangle.y * cameraHeight).toInt()
            val right = left + (rectangle.width * cameraWidth).toInt()
            val bottom = top + (rectangle.height * cameraHeight).toInt()
            return@map Rect(left, top, right, bottom)
        }
    }

}