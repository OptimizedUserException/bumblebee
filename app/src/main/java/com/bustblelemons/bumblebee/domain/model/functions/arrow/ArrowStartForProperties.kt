package com.bustblelemons.bumblebee.domain.model.functions.arrow

import android.graphics.Point
import com.bustblelemons.bumblebee.domain.model.ArrowProperties
import com.bustblelemons.bumblebee.domain.model.Position
import com.bustblelemons.bumblebee.ui.comicbubble.BubblePoints
import com.bustblelemons.bumblebee.ui.comicbubble.algebra.CollinearPointBetween
import javax.inject.Inject

class ArrowStartForProperties @Inject constructor(/*empty*/) : (ArrowProperties, BubblePoints) -> Point {

    override fun invoke(properties: ArrowProperties, bubblePoints: BubblePoints): Point {
        return when (properties.attachment) {
            Position.Left -> leftStartPosition(bubblePoints, properties.arrowOpenSizePart)
            Position.Top -> topStartPosition(bubblePoints, properties.arrowOpenSizePart)
            Position.Right -> rightStartPosition(bubblePoints, properties.arrowOpenSizePart)
            Position.Bottom -> bottomStartPosition(bubblePoints, properties.arrowOpenSizePart)
            else -> bottomStartPosition(bubblePoints, properties.arrowOpenSizePart)
        }
    }

    private fun leftStartPosition(
            bubblePoints: BubblePoints,
            arrowOpenSizePart: Float
    ): Point {
        return CollinearPointBetween(
                bubblePoints.topWest, bubblePoints.centerWest,
                1F.minus(arrowOpenSizePart)
        )
    }

    private fun topStartPosition(
            bubblePoints: BubblePoints,
            arrowOpenSizePart: Float
    ): Point {
        return CollinearPointBetween(
                bubblePoints.topWest, bubblePoints.centerTop,
                1F.minus(arrowOpenSizePart)
        )
    }

    private fun bottomStartPosition(
            bubblePoints: BubblePoints,
            arrowOpenSizePart: Float
    ): Point {
        return CollinearPointBetween(
                bubblePoints.bottomWest, bubblePoints.centerBottom,
                1F.minus(arrowOpenSizePart)
        )
    }

    private fun rightStartPosition(bubblePoints: BubblePoints, arrowOpenSizePart: Float): Point {
        return CollinearPointBetween(
                bubblePoints.topEast, bubblePoints.centerEast,
                1F.minus(arrowOpenSizePart)
        )
    }
}