package com.bustblelemons.bumblebee.domain.model

data class ArrowProperties(
        val attachment: Position = Position.Bottom,
        val pointsToward: Position = Position.Left,
        val arrowSizePart: Float = 0.125F,
        val arrowOpenSizePart: Float = 0.125F
)