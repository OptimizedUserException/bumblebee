package com.bustblelemons.bumblebee.domain.model

import android.graphics.Point

// TODO Consider moving parts of this to a native library
/*
* Implementation as in https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection */
class IntersectionPoint(
        firstLine: Pair<Point, Point>,
        secondLine: Pair<Point, Point>
) : Point() {

    init {
        val point = firstLine intersectedWith secondLine
        x = point.x
        y = point.y
    }

    private infix fun Pair<Point, Point>.intersectedWith(line: Pair<Point, Point>): Point {
        return Point(
                this intersectedAtXWith line,
                this intersectedAtYWith line
        )
    }

    private infix fun Pair<Point, Point>.intersectedAtXWith(line: Pair<Point, Point>): Int {
        return intersectedAtXWith(
                this.first.x,
                this.first.y,
                this.second.x,
                this.second.y,
                line.first.x,
                line.first.y,
                line.second.x,
                line.second.y
        )
    }

    private inline fun intersectedAtXWith(
            x1: Int,
            y1: Int,
            x2: Int,
            y2: Int,
            x3: Int,
            y3: Int,
            x4: Int,
            y4: Int
    ): Int {
        return topIntersectingAtXWith(x1, y1, x2, y2, x3, y3, x4, y4).toFloat()
                .div(bottomIntersectingAtXWith(x1, y1, x2, y2, x3, y3, x4, y4).toFloat())
                .toInt()
    }

    private fun topIntersectingAtXWith(
            x1: Int,
            y1: Int,
            x2: Int,
            y2: Int,
            x3: Int,
            y3: Int,
            x4: Int,
            y4: Int
    ): Int {
        return (((x1 * y2) - (y1 * x2)) * (x3 - x4)) - ((x1 - x2) * ((x3 * y4) - (y3 * x4)))
    }

    private fun bottomIntersectingAtXWith(
            x1: Int,
            y1: Int,
            x2: Int,
            y2: Int,
            x3: Int,
            y3: Int,
            x4: Int,
            y4: Int
    ): Int {
        return ((x1 - x2) * (y3 - y4)) - ((y1 - y2) * (x3 - x4))
    }

    private infix fun Pair<Point, Point>.intersectedAtYWith(line: Pair<Point, Point>): Int {
        return intersectedAtYWith(
                this.first.x,
                this.first.y,
                this.second.x,
                this.second.y,
                line.first.x,
                line.first.y,
                line.second.x,
                line.second.y
        )
    }

    private inline fun intersectedAtYWith(
            x1: Int,
            y1: Int,
            x2: Int,
            y2: Int,
            x3: Int,
            y3: Int,
            x4: Int,
            y4: Int
    ): Int {
        return topIntersectingAtYWith(x1, y1, x2, y2, x3, y3, x4, y4).toFloat()
                .div(bottomIntersectingAtYWith(x1, y1, x2, y2, x3, y3, x4, y4).toFloat())
                .toInt()
    }

    private fun topIntersectingAtYWith(
            x1: Int,
            y1: Int,
            x2: Int,
            y2: Int,
            x3: Int,
            y3: Int,
            x4: Int,
            y4: Int
    ): Int {
        return ((((x1 * y2) - (y1 * x2)) * (y3 - y4)) - ((y1 - y2) * ((x3 * y4) - (y3 * x4))))
    }

    private fun bottomIntersectingAtYWith(
            x1: Int,
            y1: Int,
            x2: Int,
            y2: Int,
            x3: Int,
            y3: Int,
            x4: Int,
            y4: Int
    ): Int {
        return (((x1 - x2) * (y3 - y4)) - ((y1 - y2) * (x3 - x4)))
    }
}