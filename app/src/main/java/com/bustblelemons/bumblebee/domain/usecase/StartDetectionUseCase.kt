package com.bustblelemons.bumblebee.domain.usecase

import com.bustblelemons.bumblebee.repository.facesdetection.FacesDetectedRepository
import io.fotoapparat.view.CameraRenderer
import io.reactivex.Completable
import javax.inject.Inject

class StartDetectionUseCase constructor(
        override val rxSchedulers: RxSchedulers,
        private val facesDetectedRepository: FacesDetectedRepository,
        private val cameraRenderer: CameraRenderer?
) : CompletableUseCase {

    @Inject constructor(rxSchedulers: RxSchedulers, facesDetectedRepository: FacesDetectedRepository)
            : this(rxSchedulers, facesDetectedRepository, null)

    fun renderer(cameraRenderer: CameraRenderer): StartDetectionUseCase {
        return StartDetectionUseCase(this.rxSchedulers, this.facesDetectedRepository, cameraRenderer)
    }

    override val rxStream: Completable
        get() {
            if (cameraRenderer == null) throw IllegalArgumentException("cameraRenderer has to be set")

            return Completable.fromAction { facesDetectedRepository.start(cameraRenderer) }
        }
}