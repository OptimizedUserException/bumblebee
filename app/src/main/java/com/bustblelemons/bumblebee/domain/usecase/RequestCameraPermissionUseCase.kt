package com.bustblelemons.bumblebee.domain.usecase

import android.Manifest
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.Observable
import javax.inject.Inject

class RequestCameraPermissionUseCase @Inject constructor(
        private val rxPermissions: RxPermissions, override val rxSchedulers: RxSchedulers
) : ObservableUseCase<Boolean> {

    override val rxStream: Observable<Boolean>
        get() = rxPermissions.request(Manifest.permission.CAMERA)

}