package com.bustblelemons.bumblebee.domain.usecase

import io.fotoapparat.facedetector.Rectangle
import javax.inject.Inject

class RectangleComparator @Inject constructor() : Comparator<Rectangle> {
    override fun compare(left: Rectangle, right: Rectangle): Int {
        return left.field.compareTo(right.field)
    }

    private inline val Rectangle.field: Float
        get() = width * height
}