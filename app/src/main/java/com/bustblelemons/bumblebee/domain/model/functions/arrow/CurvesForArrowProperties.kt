package com.bustblelemons.bumblebee.domain.model.functions.arrow

import com.bustblelemons.bumblebee.domain.model.Position
import com.bustblelemons.bumblebee.ui.comicbubble.BubbleArrow
import com.bustblelemons.bumblebee.ui.comicbubble.BubblePoints
import com.bustblelemons.bumblebee.ui.comicbubble.math.QuadraticBezierCurve
import javax.inject.Inject

class CurvesForArrowProperties @Inject constructor() : (
        Position,
        BubblePoints,
        BubbleArrow
) -> Array<QuadraticBezierCurve> {

    override fun invoke(
            arrowAttachment: Position,
            bubblePoints: BubblePoints,
            bubbleArrow: BubbleArrow
    ): Array<QuadraticBezierCurve> {
        return when (arrowAttachment) {
            Position.Left -> left(bubbleArrow, bubblePoints)
            Position.Top -> top(bubbleArrow, bubblePoints)
            Position.Right -> right(bubbleArrow, bubblePoints)
            Position.Bottom, Position.None -> bottom(bubbleArrow, bubblePoints)
        }
    }

    private fun left(bubbleArrow: BubbleArrow, bubblePoints: BubblePoints) =
            arrayOf(
                    bubbleArrow.curveOpen,
                    bubbleArrow.curveClose,
                    bubblePoints.bottomLeftCurve,
                    bubblePoints.bottomRightCurve,
                    bubblePoints.topRightCurve,
                    bubblePoints.topLeftCurve
            )


    private fun top(bubbleArrow: BubbleArrow, bubblePoints: BubblePoints) =
            arrayOf(
                    bubbleArrow.curveOpen,
                    bubbleArrow.curveClose,
                    bubblePoints.topRightCurveClockWise,
                    bubblePoints.bottomRightCurveClockWise,
                    bubblePoints.bottomLeftCurveClockWise,
                    bubblePoints.topLeftCurveClockWise
            )


    private fun right(bubbleArrow: BubbleArrow, bubblePoints: BubblePoints) =
            arrayOf(
                    bubbleArrow.curveOpen,
                    bubbleArrow.curveClose,
                    bubblePoints.bottomRightCurveClockWise,
                    bubblePoints.bottomLeftCurveClockWise,
                    bubblePoints.topLeftCurveClockWise,
                    bubblePoints.topRightCurveClockWise
            )


    private fun bottom(bubbleArrow: BubbleArrow, bubblePoints: BubblePoints) =
            arrayOf(
                    bubbleArrow.curveOpen,
                    bubbleArrow.curveClose,
                    bubblePoints.bottomRightCurve,
                    bubblePoints.topRightCurve,
                    bubblePoints.topLeftCurve,
                    bubblePoints.bottomLeftCurve
            )

}
