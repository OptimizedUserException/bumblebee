package com.bustblelemons.bumblebee.domain.model.functions.arrow

import android.graphics.Rect
import com.bustblelemons.bumblebee.domain.model.Position
import javax.inject.Inject

class ArrowBoundsFromProperties @Inject constructor(/*empty*/) : ((Position, Rect, Rect) -> Rect) {

    override fun invoke(arrowAttached: Position, totalBounds: Rect, bubbleBounds: Rect): Rect {
        return Rect(
                if (arrowAttached == Position.Right) bubbleBounds.right else totalBounds.left,
                if (arrowAttached == Position.Bottom) bubbleBounds.bottom else totalBounds.top,
                if (arrowAttached == Position.Left) bubbleBounds.left else totalBounds.right,
                if (arrowAttached == Position.Top) bubbleBounds.top else totalBounds.bottom
        )
    }
}