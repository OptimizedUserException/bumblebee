package com.bustblelemons.bumblebee.domain.model.functions.arrow

import android.graphics.Point
import com.bustblelemons.bumblebee.domain.model.ArrowProperties
import com.bustblelemons.bumblebee.domain.model.Position
import com.bustblelemons.bumblebee.ui.comicbubble.algebra.CollinearPointBetween
import javax.inject.Inject

class ArrowTowardCloseForProperties @Inject constructor() : (ArrowProperties, Point, Point) -> Point() {

    override fun invoke(properties: ArrowProperties, end: Point, point: Point): Point {
        return when (properties.attachment) {
            Position.Left -> leftTowards(properties, end, point)
            Position.Top -> topTowards(properties, end, point)
            Position.Right -> rightTowards(properties, end, point)
            Position.Bottom -> bottomTowards(properties, end, point)
            Position.None -> bottomTowards(properties, end, point)
        }
    }

    private fun leftTowards(properties: ArrowProperties, end: Point, point: Point): Point {
        return if (properties.pointsToward == Position.Top)
            Point(point.x, end.y)
        else if (properties.pointsToward == Position.Bottom)
            CollinearPointBetween(
                    Point(point.x, end.y),
                    end
            )
        else
            CollinearPointBetween(point, end)
    }

    private fun topTowards(properties: ArrowProperties, end: Point, point: Point): Point {
        return if (properties.pointsToward == Position.Left)
            Point(end.x, point.y)
        else if (properties.pointsToward == Position.Right)
            CollinearPointBetween(
                    Point(end.x, point.y),
                    end
            )
        else
            CollinearPointBetween(end, point)
    }

    private fun rightTowards(properties: ArrowProperties, end: Point, point: Point): Point {
        return if (properties.pointsToward == Position.Top)
            Point(point.x, end.y)
        else if (properties.pointsToward == Position.Bottom)
            CollinearPointBetween(
                    end,
                    Point(point.x, end.y)
            )
        else
            CollinearPointBetween(end, point)
    }

    private fun bottomTowards(properties: ArrowProperties, end: Point, point: Point): Point {
        return if (properties.pointsToward == Position.Left)
            Point(end.x, point.y)
        else if (properties.pointsToward == Position.Right)
            CollinearPointBetween(
                    end,
                    Point(end.x, point.y)
            )
        else
            CollinearPointBetween(point, end)

    }
}
