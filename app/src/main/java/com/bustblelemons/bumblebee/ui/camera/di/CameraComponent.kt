package com.bustblelemons.bumblebee.ui.camera.di

import com.bustblelemons.bumblebee.ui.camera.Camera
import dagger.Subcomponent
import dagger.android.AndroidInjector

@CameraScope
@Subcomponent
interface CameraComponent : AndroidInjector<Camera> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<Camera>()
}
