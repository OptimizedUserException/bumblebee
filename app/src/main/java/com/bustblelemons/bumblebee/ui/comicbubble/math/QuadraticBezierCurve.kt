package com.bustblelemons.bumblebee.ui.comicbubble.math

import android.graphics.Point

data class QuadraticBezierCurve(
        val start: Point = Point(),
        val toward: Point = Point(),
        val end: Point = Point()
)