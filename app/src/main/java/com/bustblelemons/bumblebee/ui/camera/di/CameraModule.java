package com.bustblelemons.bumblebee.ui.camera.di;

import android.content.Context;
import android.graphics.Rect;
import android.support.v4.app.FragmentActivity;
import com.bustblelemons.bumblebee.domain.model.functions.FaceToBubbleRects;
import com.bustblelemons.bumblebee.domain.model.functions.ThreeByThreeFaceToBubbleRects;
import com.bustblelemons.bumblebee.ui.camera.Camera;
import com.bustblelemons.bumblebee.ui.camera.CameraView;
import com.bustblelemons.bumblebee.ui.camera.FacesDetection;
import com.bustblelemons.bumblebee.ui.cameragrid.CameraGrid;
import com.bustblelemons.bumblebee.ui.cameragrid.ThreeByThreeCameraGrid;
import com.tbruyelle.rxpermissions2.RxPermissions;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import io.fotoapparat.Fotoapparat;
import io.fotoapparat.FotoapparatBuilder;
import io.fotoapparat.facedetector.FaceDetector;
import io.fotoapparat.facedetector.Rectangle;
import io.reactivex.BackpressureStrategy;
import io.reactivex.subjects.BehaviorSubject;

@Module
public abstract class CameraModule {

    @CameraScope
    @Provides
    static CameraView bindsView() {
        return new CameraView();
    }

    @CameraScope
    @Binds
    abstract Context bindsContext(Camera activity);

    @CameraScope
    @Binds
    abstract FragmentActivity bindsActivity(Camera camera);

    @CameraScope
    @Provides
    static FotoapparatBuilder provideFotoAparatBuilder(
            Context context,
            FacesDetection faceDetector
    ) {
        return Fotoapparat.with(context)
                .frameProcessor(faceDetector);
    }

    @CameraScope
    @Provides
    static FacesDetection provideFacesDetection(FaceDetector faceDetector) {
        return new FacesDetection(faceDetector);
    }

    @CameraScope
    @Provides
    static FaceDetector provideFaceDetector(Context context) {
        return FaceDetector.create(context);
    }

    @CameraScope
    @Provides
    static BehaviorSubject<Rectangle[]> provideFacesDetectedSubject() {
        return BehaviorSubject.create();
    }

    @CameraScope
    @Provides
    static BackpressureStrategy provideFaceDetectionBackPressureStrategy() {
        return BackpressureStrategy.LATEST;
    }

    @CameraScope
    @Provides
    static RxPermissions provideRxPermissions(FragmentActivity activity) {
        return new RxPermissions(activity);
    }

    @CameraScope
    @Provides
    static CameraGrid provideDefaultCameraGrid() {
        return new ThreeByThreeCameraGrid(new Rect());
    }

    @CameraScope
    @Binds
    abstract FaceToBubbleRects bindsBubblesToFaceMapper(ThreeByThreeFaceToBubbleRects mapper);
}