package com.bustblelemons.bumblebee.ui.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View
import com.bustblelemons.bumblebee.ui.comicbubble.BubblePaint

class GridView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    init {
        setWillNotDraw(false)
    }

    var cells: Array<Rect> = emptyArray()

    private val paints = Paints()

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (cells.isEmpty()) return

        cells.forEachIndexed { index, cell ->
            canvas.drawRect(cell, paints.paint(index))
        }
    }

    // TODO BB-29 Move this to a GridOverlay interface that will allow to draw different grids
    class Paints(val alpha: Int = (0.3 * 256).toInt()) {

        private val colors = listOf(
                Color.BLACK, Color.BLUE, Color.CYAN,
                Color.LTGRAY, Color.GRAY, Color.DKGRAY,
                Color.BLUE, Color.MAGENTA, Color.RED
        )
        private val paints = arrayOfNulls<Paint?>(colors.size)

        fun paint(index: Int): Paint {
            if (paints[index] == null) {
                paints[index] = BubblePaint().apply {
                    color = colors[index]
                    alpha = this@Paints.alpha
                }
            }
            return paints[index]!!
        }
    }

}