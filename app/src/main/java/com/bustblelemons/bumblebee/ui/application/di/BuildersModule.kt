package com.bustblelemons.bumblebee.ui.application.di

import com.bustblelemons.bumblebee.ui.camera.Camera
import com.bustblelemons.bumblebee.ui.camera.di.CameraModule
import com.bustblelemons.bumblebee.ui.camera.di.CameraScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BuildersModule {

     @CameraScope
     @ContributesAndroidInjector(modules = [CameraModule::class])
     abstract fun contributesCameraConstructor(): Camera
}