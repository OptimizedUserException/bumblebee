package com.bustblelemons.bumblebee.ui.comicbubble

import android.graphics.Path
import android.graphics.Rect
import com.bustblelemons.bumblebee.ui.comicbubble.math.QuadraticBezierCurve
import com.bustblelemons.bumblebee.ui.quadBezier
import com.bustblelemons.bumblebee.ui.startAt

class ComicBubblePath {

    var bounds: Rect = Rect()
    private var currentPath = Path()

    val path: Path get() = currentPath

    var curves: Array<QuadraticBezierCurve> = emptyArray()
        set(value) {
            field = value
            currentPath = Path().apply {
                value.forEachIndexed { index, curve ->
                    if (index == 0)
                        startAt(curve.start)

                    if (index == value.lastIndex) quadBezier(curve.toward, field.first().start)
                    else quadBezier(curve)
                }
            }
        }
}