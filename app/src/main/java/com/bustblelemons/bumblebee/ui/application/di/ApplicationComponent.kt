package com.bustblelemons.bumblebee.ui.application.di

import com.bustblelemons.bumblebee.domain.usecase.UseCasesModule
import com.bustblelemons.bumblebee.ui.application.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            AndroidSupportInjectionModule::class,
            BuildersModule::class,
            UseCasesModule::class
        ]
)
interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: Application): Builder

        fun build(): ApplicationComponent
    }

    fun inject(app: Application)
}

