package com.bustblelemons.bumblebee.ui.comicbubble

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect

interface ComicBubble {
    val bubblePaint: Paint
    val strokePaint: Paint
    val comicBubblePath: ComicBubblePath
    var bounds: Rect

    fun draw(canvas: Canvas) {
        canvas.drawPath(comicBubblePath.path, bubblePaint)
        canvas.drawPath(comicBubblePath.path, strokePaint)
    }
}