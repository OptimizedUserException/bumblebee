package com.bustblelemons.bumblebee.ui.comicbubble

import android.graphics.Color
import android.graphics.Paint
import android.support.annotation.ColorInt

open class BubblePaint : Paint {

    constructor(@ColorInt bubbleColor: Int = DEFAULT_COLOR) {
        color = bubbleColor
        isAntiAlias = true
        isDither = true
        style = Paint.Style.FILL
    }

    companion object {
        const val DEFAULT_COLOR = Color.WHITE
    }
}