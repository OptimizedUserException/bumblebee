package com.bustblelemons.bumblebee.ui.comicbubble

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View

class BubblesView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    init {
        setWillNotDraw(false)
    }

    var bubbles = emptyArray<ComicBubble>()
        set(value) {
            field = value
            invalidate()
        }

    var onBubbleTouched: (ComicBubble) -> Unit = EMPTY_TOUCH_LISTENER

    public override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        bubbles.forEach { comicBubble -> comicBubble.draw(canvas) }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                val touchedBubble = bubbles.firstOrNull { bubble ->
                    bubble.bounds.contains(
                            event.x.toInt(),
                            event.y.toInt()
                    )
                }
                touchedBubble?.let {
                    onBubbleTouched(it)
                }
                touchedBubble != null
            }
            else -> return super.onTouchEvent(event)
        }
    }

    companion object {
        @JvmStatic val EMPTY_TOUCH_LISTENER: (ComicBubble) -> Unit = { /*empty*/ }
    }
}