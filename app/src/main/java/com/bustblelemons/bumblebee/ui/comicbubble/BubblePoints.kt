package com.bustblelemons.bumblebee.ui.comicbubble

import android.graphics.Point
import android.graphics.Rect
import com.bustblelemons.bumblebee.ui.comicbubble.algebra.CollinearPointBetween
import com.bustblelemons.bumblebee.ui.comicbubble.math.QuadraticBezierCurve

class BubblePoints {

    var bounds: Rect
        get () {
            return Rect(topWestX, topWestY, bottomEastX, bottomEastY)
        }
        set(value) {
            topWestX = value.left
            topWestY = value.top
            bottomEastX = value.right
            bottomEastY = value.bottom
            adjustCenters()
            colinearCenterEastCenterBottom.adjust(centerEast, centerBottom)
            colinearCenterWestCenterBottom.adjust(centerWest, centerBottom)
        }

    private fun adjustCenters() {
        centerBottom.adjust(bottomWest, bottomEast)
        centerTop.adjust(topWest, topEast)
        centerEast.adjust(topEast, bottomEast)
        centerWest.adjust(topWest, bottomWest)
    }

    var topWestX: Int
        get() = topWest.x
        set(value) {
            topWest.x = value
            bottomWest.x = value
        }
    var topWestY: Int
        get() = topWest.y
        set(value) {
            topWest.y = value
            topEast.y = value
        }
    var bottomEastX: Int
        get() = bottomEast.x
        set(value) {
            bottomEast.x = value
            topEast.x = value
        }
    var bottomEastY: Int
        get() = bottomEast.y
        set(value) {
            bottomEast.y = value
            bottomWest.y = value
        }

    val topWest = Point()
    val topEast = Point()
    val bottomEast = Point()
    val bottomWest = Point()

    val centerTop = CollinearPointBetween(topWest, topEast)
    val centerEast = CollinearPointBetween(topEast, bottomEast)
    val centerBottom = CollinearPointBetween(bottomWest, bottomEast)
    val centerWest = CollinearPointBetween(topWest, bottomWest)

    val innerTopWest = CollinearPointBetween(centerWest, centerTop)
    val innerTopEast = CollinearPointBetween(centerTop, centerEast)
    val colinearCenterEastCenterBottom = CollinearPointBetween(centerEast, centerBottom)
    val colinearCenterWestCenterBottom = CollinearPointBetween(centerWest, centerBottom)

    // Counter-clockwise drawn
    val bottomLeftCurve: QuadraticBezierCurve get() = QuadraticBezierCurve(centerWest, bottomWest, centerBottom)
    val topLeftCurve: QuadraticBezierCurve get() = QuadraticBezierCurve(centerTop, topWest, centerWest)
    val topRightCurve: QuadraticBezierCurve get() = QuadraticBezierCurve(centerEast, topEast, centerTop)
    val bottomRightCurve: QuadraticBezierCurve get() = QuadraticBezierCurve(centerBottom, bottomEast, centerEast)

    val bottomLeftCurveClockWise: QuadraticBezierCurve
        get() = QuadraticBezierCurve(centerBottom, bottomWest, centerWest)
    val topLeftCurveClockWise: QuadraticBezierCurve
        get() = QuadraticBezierCurve(centerWest, topWest, centerTop)
    val topRightCurveClockWise: QuadraticBezierCurve
        get() = QuadraticBezierCurve(centerTop, topEast, centerEast)
    val bottomRightCurveClockWise: QuadraticBezierCurve
        get() = QuadraticBezierCurve(centerEast, bottomEast, centerBottom)
}