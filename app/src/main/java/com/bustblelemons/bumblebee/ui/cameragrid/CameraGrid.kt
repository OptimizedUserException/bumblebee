package com.bustblelemons.bumblebee.ui.cameragrid

import android.graphics.Rect

interface CameraGrid {

    val cells: Array<Rect>
}