package com.bustblelemons.bumblebee.ui

import android.graphics.Path
import android.graphics.Point
import com.bustblelemons.bumblebee.ui.comicbubble.math.QuadraticBezierCurve

fun Path.lineTo(point: Point): Path {
    lineTo(point.x.toFloat(), point.y.toFloat())
    return this
}

fun Path.quadBezier(bezierCurve: QuadraticBezierCurve): Path {
    quadBezier(bezierCurve.toward, bezierCurve.end)
    return this
}

fun Path.quadBezier(toward: Point, endAt: Point): Path {
    quadTo(toward.x.toFloat(), toward.y.toFloat(), endAt.x.toFloat(), endAt.y.toFloat())
    return this
}

fun Path.startAt(start: Point): Path {
    moveTo(start.x.toFloat(), start.y.toFloat())
    return this
}