package com.bustblelemons.bumblebee.ui.cameragrid

import android.graphics.Rect

class ThreeByThreeCameraGrid(
        val cameraBounds: Rect
) : CameraGrid {

    private val topHorizontalLineY get() = cameraBounds.top + singleCellHeight
    private val bottomHorizontalY get() = cameraBounds.bottom - singleCellHeight
    private val leftVerticalLineX get() = cameraBounds.top + singleCellWidth
    private val rightVerticalLineX get() = cameraBounds.right - singleCellWidth

    private val singleCellHeight: Int
        get() = cameraBounds.height() / 3

    private val singleCellWidth: Int
        get() = cameraBounds.width() / 3

    override val cells: Array<Rect>
        get() = arrayOf(
                topLeft,
                topCenter,
                topRight,
                centerLeft,
                center,
                centerRight,
                bottomLeft,
                bottomCenter,
                bottomRight
        )

    val topLeft: Rect
        get() {
            return Rect(
                    cameraBounds.left,
                    cameraBounds.top,
                    leftVerticalLineX,
                    topHorizontalLineY
            )
        }

    val topCenter: Rect
        get() {
            return Rect(
                    leftVerticalLineX,
                    cameraBounds.top,
                    rightVerticalLineX,
                    topHorizontalLineY
            )
        }

    val topRight: Rect
        get() {
            return Rect(
                    rightVerticalLineX,
                    cameraBounds.top,
                    cameraBounds.right,
                    topHorizontalLineY
            )
        }

    val centerLeft: Rect
        get() {
            return Rect(
                    cameraBounds.left,
                    topHorizontalLineY,
                    leftVerticalLineX,
                    bottomHorizontalY
            )
        }

    val center: Rect
        get() {
            return Rect(
                    leftVerticalLineX,
                    topHorizontalLineY,
                    rightVerticalLineX,
                    bottomHorizontalY
            )
        }
    val centerRight: Rect
        get() {
            return Rect(
                    rightVerticalLineX,
                    topHorizontalLineY,
                    cameraBounds.right,
                    bottomHorizontalY
            )
        }

    val bottomLeft: Rect
        get() {
            return Rect(
                    cameraBounds.left,
                    bottomHorizontalY,
                    leftVerticalLineX,
                    cameraBounds.bottom
            )
        }

    val bottomCenter: Rect
        get() {
            return Rect(
                    leftVerticalLineX,
                    bottomHorizontalY,
                    rightVerticalLineX,
                    cameraBounds.bottom
            )
        }

    val bottomRight: Rect
        get() {
            return Rect(
                    rightVerticalLineX,
                    bottomHorizontalY,
                    cameraBounds.right,
                    cameraBounds.bottom
            )
        }
}