package com.bustblelemons.bumblebee.ui.camera

import io.fotoapparat.facedetector.Rectangle

interface OnFacesDetected {
    fun onFacesDetected(faces: List<Rectangle>)
}