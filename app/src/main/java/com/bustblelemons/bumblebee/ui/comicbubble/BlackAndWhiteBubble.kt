package com.bustblelemons.bumblebee.ui.comicbubble

import android.graphics.Paint
import android.graphics.Rect

data class BlackAndWhiteBubble(
        override val bubblePaint: Paint = BubblePaint(),
        override val strokePaint: Paint = StrokePaint()
) : ComicBubble {

    override val comicBubblePath = ComicBubblePath()

    override var bounds: Rect = Rect()
        set(value) {
            field = value
            comicBubblePath.bounds = field
        }
}