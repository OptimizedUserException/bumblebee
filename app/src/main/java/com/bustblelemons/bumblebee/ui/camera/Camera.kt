package com.bustblelemons.bumblebee.ui.camera

import android.content.Context
import android.content.Intent
import android.view.View
import com.infullmobile.android.infullmvp.InFullMvpActivity
import dagger.android.AndroidInjection
import javax.inject.Inject

class Camera : InFullMvpActivity<CameraPresenter, CameraView>() {

    @Inject
    lateinit var cameraPresenter: CameraPresenter
    @Inject
    lateinit var cameraView: CameraView

    override val presenter: CameraPresenter get() = cameraPresenter
    override val presentedView: CameraView get() = cameraView

    override fun injectIntoGraph() {
        AndroidInjection.inject(this)
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart()
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) hideSystemUI()
    }

    private fun hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE
                // Set the content to appear under the system bars so that the
                // content doesn't resize when the system bars hide and show.
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                // Hide the nav bar and status bar
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN)
    }

    private fun showSystemUI() {
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
    }

    companion object {
        fun getIntent(context: Context): Intent = Intent(context, Camera::class.java)
    }
}
