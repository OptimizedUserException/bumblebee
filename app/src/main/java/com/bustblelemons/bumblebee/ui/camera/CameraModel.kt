package com.bustblelemons.bumblebee.ui.camera

import android.graphics.Rect
import com.bustblelemons.bumblebee.domain.model.BubbleWithDirection
import com.bustblelemons.bumblebee.domain.usecase.*
import com.bustblelemons.bumblebee.ui.comicbubble.ComicBubble
import io.fotoapparat.facedetector.Rectangle
import io.fotoapparat.view.CameraRenderer
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import javax.inject.Inject

class CameraModel @Inject constructor(
        private val mapBubblesUseCase: MapBubblesUseCase,
        private val startDetectionUseCase: StartDetectionUseCase,
        private val stopPlacingBubblesUseCases: StopPlacingBubblesUseCases,
        private val requestCameraPermissionUseCase: RequestCameraPermissionUseCase,
        private val provideKnownBoundsUseCase: ProvideKnownCameraBoundsUseCase,
        private val faceDetectionRectanglesUseCase: FaceDetectionRectanglesUseCase,
        private val persistBubbleInMemoryUseCase: PersistBubbleInMemoryUseCase,
        private val isBubblePersistedInMemoryUseCase: IsBubblePersistedInMemoryUseCase,
        private val removeBubbleFromMemory: RemoveBubbleFromMemory
) {

    fun requestCameraPermissions(): Observable<Boolean> {
        return requestCameraPermissionUseCase.rxScheduledStream
    }

    fun placeBubbles(): Flowable<Array<BubbleWithDirection>> {
        return mapBubblesUseCase
                .rxScheduledStream
    }

    fun stopPlacingBubbles(): Completable {
        return stopPlacingBubblesUseCases.rxScheduledStream
    }

    fun startDetection(cameraRenderer: CameraRenderer): Completable {
        return startDetectionUseCase.renderer(cameraRenderer).rxScheduledStream
    }

    fun provideCameraBounds(cameraBounds: Rect): Completable {
        return provideKnownBoundsUseCase.cameraBounds(cameraBounds).rxScheduledStream
    }

    fun showFaceDetection(): Flowable<List<Rectangle>> {
        return faceDetectionRectanglesUseCase.rxScheduledStream
    }

    fun persistBubbleInMemory(bubble: ComicBubble) = persistBubbleInMemoryUseCase(bubble)

    fun isBubblePersisted(comicBubble: ComicBubble): Boolean = isBubblePersistedInMemoryUseCase(comicBubble)

    fun removeFromMemory(comicBubble: ComicBubble)  = removeBubbleFromMemory(comicBubble)
}