package com.bustblelemons.bumblebee.ui.camera

import android.support.annotation.LayoutRes
import com.bustblelemons.bumblebee.R
import com.bustblelemons.bumblebee.ui.cameragrid.CameraGrid
import com.bustblelemons.bumblebee.ui.comicbubble.BubblesView
import com.bustblelemons.bumblebee.ui.comicbubble.ComicBubble
import com.bustblelemons.bumblebee.ui.views.GridView
import com.bustblelemons.bumblebee.ui.views.onEachBoundsKnown
import com.infullmobile.android.infullmvp.PresentedActivityView
import io.fotoapparat.facedetector.Rectangle
import io.fotoapparat.facedetector.view.RectanglesView
import javax.inject.Inject
import io.fotoapparat.view.CameraView as FotoAparatView

class CameraView @Inject constructor() : PresentedActivityView<CameraPresenter>() {

    @LayoutRes
    override val layoutResId = R.layout.activity_camera

    val fotoAparatView: FotoAparatView by bindView(R.id.cameraView)
    val bubblesView: BubblesView by bindView(R.id.bubblesView)
    val gridView: GridView by bindView(R.id.gridView)
    val rectanglesView: RectanglesView by bindView(R.id.rectanglesView)

    override fun onViewsBound() {
        gridView.onEachBoundsKnown { _, rect ->
            presenter.onBoundsKnown(rect)
        }
        bubblesView.onBubbleTouched = { presenter.onBubbleTouched(it) }
    }

    fun displayCameraGrid(grid: CameraGrid) {
        gridView.cells = grid.cells
    }

    fun displayBubbles(bubbles: Array<ComicBubble>) {
        bubblesView.bubbles = bubbles
    }

    fun displayFaceDetection(faceRectangles: List<Rectangle>) {
        rectanglesView.setRectangles(faceRectangles)
    }

    fun unbind() {
        bubblesView.onBubbleTouched = BubblesView.EMPTY_TOUCH_LISTENER
    }
}
