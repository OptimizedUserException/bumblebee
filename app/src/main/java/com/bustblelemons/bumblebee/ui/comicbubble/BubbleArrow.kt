package com.bustblelemons.bumblebee.ui.comicbubble

import android.graphics.Point
import com.bustblelemons.bumblebee.ui.comicbubble.math.QuadraticBezierCurve

data class BubbleArrow(
        val start: Point,
        val point: Point,
        val end: Point,
        val towardOpen: Point,
        val towardClose: Point
) {

    val curveOpen: QuadraticBezierCurve
        get() = QuadraticBezierCurve(start, towardOpen, point)

    val curveClose: QuadraticBezierCurve
        get() = QuadraticBezierCurve(point, towardClose, end)
}