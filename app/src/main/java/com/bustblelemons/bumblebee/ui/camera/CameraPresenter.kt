package com.bustblelemons.bumblebee.ui.camera

import android.annotation.SuppressLint
import android.graphics.Rect
import android.net.Uri
import android.os.Bundle
import com.bustblelemons.bumblebee.domain.model.BubbleWithDirection
import com.bustblelemons.bumblebee.ui.cameragrid.ThreeByThreeCameraGrid
import com.bustblelemons.bumblebee.ui.comicbubble.ComicBubble
import com.bustblelemons.bumblebee.ui.register
import com.infullmobile.android.infullmvp.Presenter
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class CameraPresenter @Inject constructor(
        private val model: CameraModel,
        view: CameraView
) : Presenter<CameraView>(view) {

    internal val compositeDisposable = CompositeDisposable()

    override fun bind(intentBundle: Bundle, savedInstanceState: Bundle, intentData: Uri?) {
        /*empty*/
    }

    @SuppressLint("CheckResult")
    fun onStop() {
        model.stopPlacingBubbles().subscribe({ /*NO OP*/ }, Throwable::printStackTrace)
    }

    override fun unbind() {
        super.unbind()
        presentedView.unbind()
        compositeDisposable.dispose()
    }

    fun onStart() {
        startDetection()
    }

    fun startDetection() {
        model.requestCameraPermissions()
                .subscribe(
                        { granted -> if (granted) detectAndPlaceBubbles() else startDetection() },
                        Throwable::printStackTrace
                ).register(compositeDisposable)
    }

    private fun detectAndPlaceBubbles() {
        model.showFaceDetection()
                .subscribe(
                        { faceRectangles -> presentedView.displayFaceDetection(faceRectangles) },
                        Throwable::printStackTrace
                ).register(compositeDisposable)
        model.startDetection(presentedView.fotoAparatView)
                .andThen(model.placeBubbles())
                .subscribe(
                        { bubbles -> if (bubbles.isNotEmpty()) handleBubbles(bubbles) },
                        Throwable::printStackTrace
                ).register(compositeDisposable)
    }

    private fun handleBubbles(bubbles: Array<BubbleWithDirection>) {
        presentedView.displayBubbles(bubbles.map { it.bubble }.toTypedArray())
    }

    fun onBoundsKnown(rect: Rect) {
        model.provideCameraBounds(rect)
                .subscribe(
                        { /*empty*/ },
                        Throwable::printStackTrace
                )
                .register(compositeDisposable)
        presentedView.displayCameraGrid(ThreeByThreeCameraGrid(rect))
    }

    fun onBubbleTouched(comicBubble: ComicBubble) {
        if (model.isBubblePersisted(comicBubble))
            model.removeFromMemory(comicBubble)
        else
            model.persistBubbleInMemory(comicBubble)

    }
}