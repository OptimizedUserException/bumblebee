package com.bustblelemons.bumblebee.ui.views

import android.graphics.Rect
import android.view.View

inline fun View.onEachBoundsKnown(
        crossinline callback: ((View, Rect) -> Unit)
): View.OnLayoutChangeListener {
    val listener = View.OnLayoutChangeListener { view, left, top, right, bottom,
                                                 _, _, _, _ ->
        callback(view, Rect(left, top, right, bottom))
    }
    this.addOnLayoutChangeListener(listener)
    return listener
}