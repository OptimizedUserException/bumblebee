package com.bustblelemons.bumblebee.ui.comicbubble

import android.graphics.*
import android.graphics.drawable.Drawable
import android.support.annotation.ColorInt
import android.support.annotation.Dimension

class BubbleDrawable(
        @ColorInt private val bubbleColor: Int = Color.WHITE,
        @ColorInt private val bubbleStrokeColor: Int = Color.BLACK,
        @Dimension(unit = Dimension.PX) private val bubbleStrokeWidth: Float = 5.0F,
        speechPart: Float = 0.75F,
        speechArrowOpening: Float = 0.2F
) : Drawable() {

    private var bubbleStrokePaint: Paint = BubblePaint(bubbleColor)
    private var bubblePaint: Paint = StrokePaint(bubbleStrokeColor, bubbleStrokeWidth)
    private val comicBubblePath: ComicBubblePath = ComicBubblePath()

    constructor(
            @ColorInt bubbleColor: Int = Color.WHITE,
            @ColorInt bubbleStrokeColor: Int = Color.BLACK,
            @Dimension(unit = Dimension.PX) bubbleStrokeWidth: Float = 5.0F
    ) : this(
            bubbleColor,
            bubbleStrokeColor,
            bubbleStrokeWidth,
            0.75F,
            0.2f
    )

    init {
        initPath(bounds)
    }

    override fun onBoundsChange(bounds: Rect) {
        super.onBoundsChange(bounds)
        initPath(bounds)
    }

    private fun initPath(bounds: Rect) {
        bounds.inset(bubbleStrokeWidth.toInt(), bubbleStrokeWidth.toInt())
        comicBubblePath.bounds = bounds
    }

    override fun draw(canvas: Canvas) {
        canvas.drawPath(comicBubblePath.path, bubblePaint)
        canvas.drawPath(comicBubblePath.path, bubbleStrokePaint)
    }

    override fun setAlpha(alpha: Int) {
        this.bubblePaint.alpha = alpha
        this.bubbleStrokePaint.alpha = alpha
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        this.bubblePaint.colorFilter = colorFilter
        this.bubbleStrokePaint.colorFilter = colorFilter
    }

    override fun getOpacity(): Int {
        return PixelFormat.OPAQUE
    }
}
