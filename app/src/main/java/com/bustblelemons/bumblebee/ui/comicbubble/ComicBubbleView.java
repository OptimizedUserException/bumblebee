package com.bustblelemons.bumblebee.ui.comicbubble;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import com.bustblelemons.bumblebee.R;

public class ComicBubbleView extends View {

    public ComicBubbleView(final Context context) {
        super(context);
        init(context, null);
    }

    public ComicBubbleView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ComicBubbleView(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ComicBubbleView(
            final Context context,
            final AttributeSet attrs,
            final int defStyleAttr,
            final int defStyleRes
    ) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    protected void init(final Context context, final AttributeSet attrs) {
        setFocusable(true); /* necessary for getting the touch events? */
        processAttributes(context, attrs);

    }

    private void processAttributes(final Context context, final AttributeSet attrs) {
        if (attrs != null) {
            final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ComicBubbleView);

            /* Bubble */
            final int bubbleColor = a.getColor(R.styleable.ComicBubbleView_bubbleColor, Color.WHITE);
            final int bubbleStrokeColor = a.getColor(R.styleable.ComicBubbleView_bubbleStrokeColor, Color.BLACK);
            final int bubbleStrokeWidth = a.getDimensionPixelSize(R.styleable.ComicBubbleView_bubbleStrokeWidth, 5);
            final float bubbleArrowSizeOpening = a.getFloat(R.styleable.ComicBubbleView_bubbleSpeechArrowOpening, 0.1F);
            final float speechPercentage = a.getFloat(R.styleable.ComicBubbleView_speechPercentage, 0.75F);
            final BubbleDrawable bubbleDrawable = new BubbleDrawable(
                    bubbleColor,
                    bubbleStrokeColor,
                    bubbleStrokeWidth,
                    speechPercentage,
                    bubbleArrowSizeOpening
            );
            setBackground(bubbleDrawable);
            a.recycle();
        } else {
            setBackground(new BubbleDrawable());
        }
    }
}
