package com.bustblelemons.bumblebee.ui

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

fun Disposable.register(compositeDisposable: CompositeDisposable) {
    compositeDisposable.add(compositeDisposable)
}