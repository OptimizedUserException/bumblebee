package com.bustblelemons.bumblebee.ui.comicbubble

import android.graphics.Color
import android.graphics.Paint
import android.support.annotation.ColorInt

class StrokePaint : Paint {

    constructor(
            @ColorInt bubbleStrokeColor: Int = DEFAULT_COLOR,
            bubbleStrokeWidth: Float = DEFAULT_WIDTH
    ) {
        color = bubbleStrokeColor
        style = Paint.Style.STROKE
        strokeWidth = bubbleStrokeWidth
    }

    companion object {
        const val DEFAULT_COLOR = Color.BLACK
        const val DEFAULT_WIDTH = 5F
    }
}