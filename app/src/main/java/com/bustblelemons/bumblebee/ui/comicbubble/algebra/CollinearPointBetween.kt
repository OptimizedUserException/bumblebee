package com.bustblelemons.bumblebee.ui.comicbubble.algebra

import android.graphics.Point

class CollinearPointBetween(
        private val start: Point,
        private val end: Point,
        private val percentage: Float = 0.5F
) : Point() {


    init {
        x = collinearMiddleX(this.start, this.end)
        y = collinearMiddleY(this.start, this.end)
    }

    fun adjust(start: Point, end: Point) {
        x = collinearMiddleX(start, end)
        y = collinearMiddleY(start, end)
    }

    private inline fun collinearMiddleX(start: Point, end: Point): Int {
        return if (start.x == end.x) start.x
        else start.x + (Math.abs(start.x - end.x) * percentage).toInt()
    }

    private inline fun collinearMiddleY(start: Point, end: Point): Int {
        return if (start.y == end.y) start.y
        else start.y + (Math.abs(start.y - end.y) * percentage).toInt()
    }
}