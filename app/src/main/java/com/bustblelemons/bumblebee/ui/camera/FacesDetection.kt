package com.bustblelemons.bumblebee.ui.camera

import io.fotoapparat.facedetector.FaceDetector
import io.fotoapparat.facedetector.Rectangle
import io.fotoapparat.preview.Frame
import javax.inject.Inject

class FacesDetection @Inject constructor(
        private val facesDetector: FaceDetector
) : (io.fotoapparat.preview.Frame) -> kotlin.Unit {

    var facesDetected: OnFacesDetected = EMPTY_FACES_DETECTED

    override fun invoke(frame: Frame) {
        facesDetected.onFacesDetected(facesInFrame(frame))
    }

    private inline fun facesInFrame(frame: Frame): List<Rectangle> {
        return facesDetector
                .detectFaces(frame.image, frame.size.width, frame.size.height, frame.rotation)
                .filter { it.height > 0 && it.width > 0 }
    }

    companion object {
        @JvmStatic
        val EMPTY_FACES_DETECTED = object : OnFacesDetected {
            override fun onFacesDetected(faces: List<Rectangle>) {
                /*EMPTY*/
            }
        }
    }
}