package com.bustblelemons.bumblebee

import android.graphics.Rect
import com.bustblelemons.bumblebee.ui.comicbubble.BlackAndWhiteBubble
import com.bustblelemons.bumblebee.ui.comicbubble.ComicBubble

object ComicBubblesFactory {

    fun someBubbles(): List<ComicBubble> {
        return (0..10)
                .map { oneBubble.apply { bounds = Rect(it, it * 2, it * 3, it * 4) } }
    }

    val oneBubble: ComicBubble get() = BlackAndWhiteBubble()
}