package com.bustblelemons.bumblebee

import org.mockito.Mockito

// Kotlin Hack Extensions
fun <T> anyObject(): T {
    Mockito.anyObject<T>()
    return uninitialized()
}

private fun <T> uninitialized(): T = null as T

