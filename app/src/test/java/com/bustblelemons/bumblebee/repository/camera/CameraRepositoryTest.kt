package com.bustblelemons.bumblebee.repository.camera

import android.graphics.Rect
import com.bustblelemons.bumblebee.ui.cameragrid.ThreeByThreeCameraGrid
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class CameraRepositoryTest {

    private lateinit var repository: CameraRepository
    private val initialBounds = Rect(10, 1, 1, 10)
    private val initialCameraGrid = ThreeByThreeCameraGrid(initialBounds)

    @Before fun setup() {
        repository = CameraRepository(initialBounds, initialCameraGrid)
    }

    @Test fun shouldInitViaConstructorCorrectly() {
        // when
        // via setup()

        // then
        assertEquals(initialBounds, repository.cameraBounds)
        assertEquals(initialCameraGrid, repository.cameraGrid)
    }

    @Test fun shouldRegisterLatestCameraBounds() {
        // given
        val expectedLatestBounds = Rect(1000, 1000, 1000, 1000)

        // expected
        assertEquals(initialBounds, repository.cameraBounds)

        // when
        repository.latestCameraBounds(expectedLatestBounds)

        // then
        assertEquals(expectedLatestBounds, repository.cameraBounds)
    }

    @Test fun shouldRegisterLatestCameraGrid() {
        // given
        val expectedLatestCameraGrid = ThreeByThreeCameraGrid(Rect(1000, 1000, 1000, 1000))

        // expected
        assertEquals(initialBounds, repository.cameraBounds)

        // when
        repository.latestCameraGrid(expectedLatestCameraGrid)

        // then
        assertEquals(expectedLatestCameraGrid, repository.cameraGrid)
    }
}