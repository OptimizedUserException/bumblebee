package com.bustblelemons.bumblebee.repository.facesdetection

import com.bustblelemons.bumblebee.RectanglesFactory
import com.bustblelemons.bumblebee.anyObject
import com.bustblelemons.bumblebee.ui.camera.FacesDetection
import com.bustblelemons.bumblebee.ui.cameragrid.CameraGrid
import io.fotoapparat.Fotoapparat
import io.fotoapparat.FotoapparatBuilder
import io.fotoapparat.facedetector.Rectangle
import io.fotoapparat.view.CameraRenderer
import io.reactivex.BackpressureStrategy
import io.reactivex.subjects.BehaviorSubject
import org.junit.Before
import org.junit.Test
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class FacesDetectedRepositoryTest {

    lateinit var facesDetectedRepository: FacesDetectedRepository
    val facesDetectedSubject: BehaviorSubject<Array<Rectangle>> = spy(BehaviorSubject.create())
    private val pressureStrategy: BackpressureStrategy = BackpressureStrategy.LATEST
    @Mock
    lateinit var facesDetection: FacesDetection
    @Mock
    lateinit var fotoapparatBuilder: FotoapparatBuilder
    @Mock lateinit var cameraRenderer: CameraRenderer

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        given(fotoapparatBuilder.into(anyObject())).willReturn(fotoapparatBuilder)
        facesDetectedRepository = FacesDetectedRepository(
                facesDetectedSubject,
                pressureStrategy,
                facesDetection,
                fotoapparatBuilder
        )
    }

    @Test
    fun shouldSetupFacesDetectionCallbackCorrectly() {
        // when
        // init via setup()

        // then
        verify(facesDetection).facesDetected = facesDetectedRepository
    }

    @Test
    fun shouldForwardAllFacesDetectedToSubject() {
        // given
        val someFaces = RectanglesFactory.someFaces()
        val testSubscriber = facesDetectedRepository.detectFaces.test()

        // when
        facesDetectedRepository.onFacesDetected(someFaces)

        // then
        testSubscriber.assertValue { facesWeGot -> facesWeGot contentEquals someFaces.toTypedArray() }

    }

    @Test fun shouldStartCorrectly() {
        // given
        val mockedFotoaparat = mock(Fotoapparat::class.java)
        given(fotoapparatBuilder.build()).willReturn(mockedFotoaparat)
        val someRenderer = mock(CameraRenderer::class.java)

        // when
        facesDetectedRepository.start(someRenderer)

        // then
        verify(mockedFotoaparat).start()
    }

    @Test fun shouldStopCorrectly() {
        // given
        val mockedFotoaparat = mock(Fotoapparat::class.java)
        given(fotoapparatBuilder.build()).willReturn(mockedFotoaparat)
        facesDetectedRepository.start(cameraRenderer)

        // when
        facesDetectedRepository.stop()

        // then
        verify(mockedFotoaparat).stop()
        verify(facesDetection).facesDetected = FacesDetection.EMPTY_FACES_DETECTED
        verify(facesDetectedSubject).onComplete()
    }
}