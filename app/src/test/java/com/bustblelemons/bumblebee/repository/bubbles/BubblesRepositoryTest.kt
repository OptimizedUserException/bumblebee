package com.bustblelemons.bumblebee.repository.bubbles

import com.bustblelemons.bumblebee.ComicBubblesFactory
import junit.framework.Assert.assertFalse
import junit.framework.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class BubblesRepositoryTest {

    @Test fun shouldAddCorrectly() {
        // given
        val repository = BubblesRepository()
        val comicBubble = ComicBubblesFactory.oneBubble

        // when
        repository.addBubble(comicBubble)

        // then
        assertTrue(repository.bubbles.contains(comicBubble))
    }

    @Test fun shouldRemoveCorrectly() {
        // given
        val repository = BubblesRepository()
        val bubble = ComicBubblesFactory.someBubbles()[1]
        val bubble2 = ComicBubblesFactory.someBubbles()[2]
        val bubble3 = ComicBubblesFactory.someBubbles()[3]
        repository.addBubble(bubble)
        repository.addBubble(bubble2)
        repository.addBubble(bubble3)

        // when
        repository.removeBubble(bubble)

        // then
        assertFalse(repository.bubbles.contains(bubble))
    }
}