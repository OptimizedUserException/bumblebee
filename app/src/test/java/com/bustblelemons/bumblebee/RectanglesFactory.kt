package com.bustblelemons.bumblebee

import io.fotoapparat.facedetector.Rectangle

object RectanglesFactory {
    fun someFaces(): List<Rectangle> {
        return listOf(
                Rectangle(0.2F, 0.1F, 100F, 200F),
                Rectangle(0.4F, 0.15F, 100F, 200F),
                Rectangle(0.5F, 0.25F, 100F, 200F)
        )
    }
}