package com.bustblelemons.bumblebee.ui.comicbubble.algebra

import android.graphics.Point
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class CollinearPointBetweenTest {

    @Test fun shouldCalculateXCorrectly() {
        // given
        val start = Point(2, 2)
        val end = Point(6, 6)
        val expected = 4

        // when
        val collinear = CollinearPointBetween(start, end)

        // then
        assertEquals(expected, collinear.x)
    }

    @Test fun shouldCalculateYCorrectly() {
        // given
        val start = Point(2, 4)
        val end = Point(6, 8)
        val expected = 6

        // when
        val collinear = CollinearPointBetween(start, end)

        // then
        assertEquals(expected, collinear.y)
    }

    @Test fun shouldAccountForPercentageCorrectly() {
        // given
        val start = Point(20, 80)
        val end = Point(120, 180)
        val expectedX = 45
        val expectedY = 105
        val percentage = 0.25F

        // when
        val collinear = CollinearPointBetween(start, end, percentage)

        // then
        assertEquals(expectedX, collinear.x)
        assertEquals(expectedY, collinear.y)
    }
}