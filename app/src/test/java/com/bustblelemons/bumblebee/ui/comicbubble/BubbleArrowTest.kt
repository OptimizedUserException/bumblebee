package com.bustblelemons.bumblebee.ui.comicbubble

import android.graphics.Point
import com.bustblelemons.bumblebee.ui.comicbubble.math.QuadraticBezierCurve
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class BubbleArrowTest {

    @Test fun shouldHaveProperCurveOpen() {
        // given
        val start = Point(120, 0)
        val point = Point(0, 100)
        val end = Point(20, 0)
        val towardOpen = Point(20, 21)
        val towardClose = Point(22, 23)
        val bubbleArrow = BubbleArrow(start, point, end, towardOpen, towardClose)
        val expected = QuadraticBezierCurve(start, towardOpen, point)

        // when
        val curve = bubbleArrow.curveOpen

        // then
        assertEquals(curve, expected)
    }

    @Test fun shouldHaveProperCurveClose() {
        // given
        val start = Point(120, 0)
        val point = Point(0, 100)
        val end = Point(20, 0)
        val towardOpen = Point(20, 21)
        val towardClose = Point(22, 23)
        val bubbleArrow = BubbleArrow(start, point, end, towardOpen, towardClose)
        val expected = QuadraticBezierCurve(point, towardClose, end)

        // when
        val curve = bubbleArrow.curveClose

        // then
        assertEquals(curve, expected)
    }
}