package com.bustblelemons.bumblebee.ui.camera

import io.reactivex.Completable
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class CameraPresenterTest {

    lateinit var presenter: CameraPresenter
    @Mock lateinit var model: CameraModel
    @Mock lateinit var view: CameraView


    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        presenter = CameraPresenter(model, view)
    }

    @Test fun shouldStopBubblePlacingOnStop() {
        // given
        var completed = false
        val stopPlacingBubbles = Completable.complete().doOnComplete { completed = true }
        given(model.stopPlacingBubbles()).willReturn(stopPlacingBubbles)

        // when
        presenter.onStop()

        // then
        verify(model).stopPlacingBubbles()
        assertTrue(completed)
    }

    @Test fun shouldDisposeOnUnbind() {
        // when
        presenter.unbind()

        // then
        assertTrue(presenter.compositeDisposable.isDisposed)
    }

    @Test fun shouldPassUnbidToView() {
        // when
        presenter.unbind()

        // then
        verify(view).unbind()
    }
}