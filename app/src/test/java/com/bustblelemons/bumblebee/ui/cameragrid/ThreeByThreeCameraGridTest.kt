package com.bustblelemons.bumblebee.ui.cameragrid

import android.graphics.Rect
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class ThreeByThreeCameraGridTest {
    private val cameraLeft = 0
    private val cameraTop = 0
    private val cameraRight = 900
    private val cameraBottom = 600
    private val cameraBounds = Rect(cameraLeft, cameraTop, cameraRight, cameraBottom)
    private val thirdOfCameraBoundsWidth: Int = cameraBounds.width() / 3
    private val thirdOfCameraBoundsHeight = cameraBounds.height() / 3

    private var grid: ThreeByThreeCameraGrid = ThreeByThreeCameraGrid(cameraBounds)

    @Before fun setup() {
        grid = ThreeByThreeCameraGrid(cameraBounds)
    }

    @Test fun shouldHaveProperCellsCount() {
        // given
        val expectedSize = 9

        // when
        val actualCellsGrid = grid.cells

        // then
        assertEquals(actualCellsGrid.size, expectedSize)
    }

    @Test fun shouldCalculateTopLeftCorrectly() {
        // given
        val expectedTop = cameraTop
        val expectedLeft = cameraLeft

        // when
        val cell = grid.topLeft

        // then
        assertEquals(expectedLeft, cell.left)
        assertEquals(expectedTop, cell.top)
        assertEquals(thirdOfCameraBoundsWidth, cell.width())
        assertEquals(thirdOfCameraBoundsHeight, cell.height())
    }

    @Test fun shouldCalculateTopCenterCorrectly() {
        // given
        val expectedTop = cameraTop

        // when
        val cell = grid.topCenter

        // then
        assertEquals(thirdOfCameraBoundsWidth, cell.left)
        assertEquals(expectedTop, cell.top)
        assertEquals(thirdOfCameraBoundsWidth, cell.width())
        assertEquals(thirdOfCameraBoundsHeight, cell.height())
    }

    @Test fun shouldCalculateTopRightCorrectly() {
        // given
        val expectedLeft = thirdOfCameraBoundsWidth * 2
        val expectedTop = cameraTop

        // when
        val cell = grid.topRight

        // then
        assertEquals(expectedLeft, cell.left)
        assertEquals(expectedTop, cell.top)
        assertEquals(thirdOfCameraBoundsWidth, cell.width())
        assertEquals(thirdOfCameraBoundsHeight, cell.height())
    }

    @Test fun shouldCalculateCenterLeftCorrectly() {
        // given
        val expectedTop = cameraTop + thirdOfCameraBoundsHeight
        val expectedLeft = cameraLeft

        // when
        val cell = grid.centerLeft

        // then
        assertEquals(expectedLeft, cell.left)
        assertEquals(expectedTop, cell.top)
        assertEquals(thirdOfCameraBoundsWidth, cell.width())
        assertEquals(thirdOfCameraBoundsHeight, cell.height())
    }

    @Test fun shouldCalculateCenterCorrectly() {
        // given
        val expectedLeft = cameraLeft + thirdOfCameraBoundsWidth
        val expectedTop = cameraTop + thirdOfCameraBoundsHeight

        // when
        val cell = grid.center

        // then
        assertEquals(expectedLeft, cell.left)
        assertEquals(expectedTop, cell.top)
        assertEquals(thirdOfCameraBoundsWidth, cell.width())
        assertEquals(thirdOfCameraBoundsHeight, cell.height())
    }

    @Test fun shouldCalculateCenterRightCorrectly() {
        // given
        val expectedTop = cameraTop + thirdOfCameraBoundsHeight
        val expectedLeft = cameraLeft + (thirdOfCameraBoundsWidth * 2)

        // when
        val cell = grid.centerRight

        // then
        assertEquals(expectedLeft, cell.left)
        assertEquals(expectedTop, cell.top)
        assertEquals(thirdOfCameraBoundsWidth, cell.width())
        assertEquals(thirdOfCameraBoundsHeight, cell.height())
    }


    @Test fun shouldCalculateBottomLeftCorrectly() {
        // given
        val expectedTop = cameraTop + (thirdOfCameraBoundsHeight * 2)
        val expectedLeft = cameraLeft

        // when
        val cell = grid.bottomLeft

        // then
        assertEquals(expectedLeft, cell.left)
        assertEquals(expectedTop, cell.top)
        assertEquals(thirdOfCameraBoundsWidth, cell.width())
        assertEquals(thirdOfCameraBoundsHeight, cell.height())
    }


    @Test fun shouldCalculateBottomCenterCorrectly() {
        // given
        val expectedTop = cameraTop + (thirdOfCameraBoundsHeight * 2)
        val expectedLeft = cameraLeft + thirdOfCameraBoundsWidth

        // when
        val cell = grid.bottomCenter

        // then
        assertEquals(expectedLeft, cell.left)
        assertEquals(expectedTop, cell.top)
        assertEquals(thirdOfCameraBoundsWidth, cell.width())
        assertEquals(thirdOfCameraBoundsHeight, cell.height())
    }


    @Test fun shouldCalculateBottomRightCorrectly() {
        // given
        val expectedTop = cameraTop + (thirdOfCameraBoundsHeight * 2)
        val expectedLeft = cameraLeft + (thirdOfCameraBoundsWidth * 2)

        // when
        val cell = grid.bottomRight

        // then
        assertEquals(expectedLeft, cell.left)
        assertEquals(expectedTop, cell.top)
        assertEquals(thirdOfCameraBoundsWidth, cell.width())
        assertEquals(thirdOfCameraBoundsHeight, cell.height())
    }
}