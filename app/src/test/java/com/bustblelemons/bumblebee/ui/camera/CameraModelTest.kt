package com.bustblelemons.bumblebee.ui.camera

import android.graphics.Rect
import com.bustblelemons.bumblebee.ComicBubblesFactory
import com.bustblelemons.bumblebee.DirectedComicBubblesFactory
import com.bustblelemons.bumblebee.RectanglesFactory
import com.bustblelemons.bumblebee.anyObject
import com.bustblelemons.bumblebee.domain.usecase.*
import io.fotoapparat.view.CameraRenderer
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import org.junit.Assert.assertNull
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class CameraModelTest {

    @get:Rule val rule: MockitoRule = MockitoJUnit.rule()

    @InjectMocks lateinit var model: CameraModel
    @Mock lateinit var mapBubblesUseCase: MapBubblesUseCase
    @Mock lateinit var stopPlaceBubblesUseCase: StopPlacingBubblesUseCases
    @Mock lateinit var startDetectionUseCase: StartDetectionUseCase
    @Mock lateinit var requestCameraPermissionUseCase: RequestCameraPermissionUseCase
    @Mock lateinit var provideKnownCameraBoundsUseCase: ProvideKnownCameraBoundsUseCase
    @Mock lateinit var faceDetectionRectanglesUseCase: FaceDetectionRectanglesUseCase
    @Mock lateinit var persistBubbleInMemoryUseCase: PersistBubbleInMemoryUseCase
    @Mock lateinit var isBubblePersistedInMemoryUseCase: IsBubblePersistedInMemoryUseCase
    @Mock lateinit var removeBubbleFromMemory: RemoveBubbleFromMemory

    @Test
    fun shouldStopCorrectly() {
        // given
        given(stopPlaceBubblesUseCase.rxScheduledStream).willReturn(Completable.complete())

        // when
        val shouldBeNullThrowable = model.stopPlacingBubbles().blockingGet()

        // then
        assertNull(shouldBeNullThrowable)
    }

    @Test
    fun shouldPlaceBubblesCorrectly() {
        // given
        val items = DirectedComicBubblesFactory.someBubbles().toTypedArray()
        given(mapBubblesUseCase.rxScheduledStream).willReturn(Flowable.fromArray(items))

        // when
        val testSubscriber = model.placeBubbles().test()

        // then
        testSubscriber.assertComplete()
        testSubscriber.assertValues(items)
    }

    @Test
    fun shouldStartDetectingCorrectly() {
        // given
        val someRenderer = mock(CameraRenderer::class.java)
        given(startDetectionUseCase.rxScheduledStream).willReturn(Completable.complete())
        given(startDetectionUseCase.renderer(anyObject())).willReturn(startDetectionUseCase)

        // when
        val testSubscriber = model.startDetection(someRenderer).test()

        // then
        testSubscriber.assertComplete()
        testSubscriber.assertNoErrors()
    }

    @Test
    fun requestCameraPermissionCorrectly() {
        // given
        val expectedPermissionGrant = true
        given(requestCameraPermissionUseCase.rxScheduledStream).willReturn(Observable.just(expectedPermissionGrant))

        // when
        val testSubscriber = model.requestCameraPermissions().test()

        // then
        testSubscriber.assertComplete()
        testSubscriber.assertNoErrors()
        testSubscriber.assertValue(expectedPermissionGrant)
    }

    @Test
    fun shouldProvideCameraBoundsCorrectly() {
        // given
        val someRect = Rect(0, 0, 20, 22)
        given(provideKnownCameraBoundsUseCase.rxScheduledStream).willReturn(Completable.complete())
        given(provideKnownCameraBoundsUseCase.cameraBounds(anyObject())).willReturn(provideKnownCameraBoundsUseCase)

        // when
        val testSubscriber = model.provideCameraBounds(someRect).test()

        // then
        testSubscriber.assertComplete()
        testSubscriber.assertNoErrors()
    }

    @Test
    fun shouldReturnFaceDetectionRectanglesCorrectly() {
        // given
        val items = RectanglesFactory.someFaces()
        given(faceDetectionRectanglesUseCase.rxScheduledStream).willReturn(Flowable.fromArray(items))

        // when
        val testSubscriber = model.showFaceDetection().test()

        // then
        testSubscriber.assertComplete()
        testSubscriber.assertNoErrors()
        testSubscriber.assertValue(items)
    }

    @Test fun shouldPersistCorrectly() {
        // given
        val someBubble = ComicBubblesFactory.oneBubble

        // when
        model.persistBubbleInMemory(someBubble)

        // then
        verify(persistBubbleInMemoryUseCase).invoke(someBubble)
    }
}