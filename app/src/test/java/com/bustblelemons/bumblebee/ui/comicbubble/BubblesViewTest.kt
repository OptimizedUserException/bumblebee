package com.bustblelemons.bumblebee.ui.comicbubble

import android.graphics.Canvas
import com.bustblelemons.bumblebee.anyObject
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment

@RunWith(RobolectricTestRunner::class)
class BubblesViewTest {

    lateinit var testedView: BubblesView

    @Before fun setup() {
        testedView = BubblesView(RuntimeEnvironment.application)
    }

    @Test fun shouldBeSetupForDrawing() {
        // when
        val willNotDraw = testedView.willNotDraw()

        // then
        assertFalse(willNotDraw)
    }

    @Test fun shouldDrawBubblesCorrectly() {
        // given
        val testingBubbles = someBubbles(10)
        testedView.bubbles = testingBubbles
        val canvas = Canvas()

        // when
        testedView.onDraw(canvas)

        // then
        testedView.bubbles.forEach { bubble ->
            verify(bubble).draw(anyObject())
        }
    }

    private fun someBubbles(size: Int) = (0 until size).map { singleBubble() }.toTypedArray()

    private fun singleBubble() = mock(ComicBubble::class.java)
}