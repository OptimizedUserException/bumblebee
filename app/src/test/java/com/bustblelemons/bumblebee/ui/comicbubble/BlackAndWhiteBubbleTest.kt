package com.bustblelemons.bumblebee.ui.comicbubble

import android.graphics.Color
import android.graphics.Rect
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class BlackAndWhiteBubbleTest {

    @Test fun shouldHaveBlackStrokeColor() {
        // given
        val blackAndWhiteBubble = BlackAndWhiteBubble()

        // when
        val color = blackAndWhiteBubble.strokePaint.color

        // then
        assertEquals(color, Color.BLACK)
    }

    @Test fun shouldHaveWhiteFillColor() {
        // given
        val blackAndWhiteBubble = BlackAndWhiteBubble()

        // when
        val color = blackAndWhiteBubble.bubblePaint.color

        // then
        assertEquals(color, Color.WHITE)
    }

    @Test fun shouldHaveComicBubblePath() {
        // given
        val blackAndWhiteBubble = BlackAndWhiteBubble()

        // when
        val path = blackAndWhiteBubble.comicBubblePath

        // then
        assertNotNull(path)
    }

    @Test fun shouldHaveBounds() {
        // given
        val blackAndWhiteBubble = BlackAndWhiteBubble()
        val someBounds = Rect(0, 0, 200, 200)

        // when
        blackAndWhiteBubble.bounds = someBounds

        // then
        assertEquals(blackAndWhiteBubble.bounds, someBounds)
    }
}