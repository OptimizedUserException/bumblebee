package com.bustblelemons.bumblebee.ui.comicbubble

import android.graphics.Paint
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class BubblePaintTest {

    @Test fun shouldInnitCorrectly() {
        // when
        val paint = BubblePaint()

        // then
        assertEquals(paint.color, BubblePaint.DEFAULT_COLOR)
        assertTrue(paint.isAntiAlias)
        assertTrue(paint.isDither)
        assertEquals(paint.style, Paint.Style.FILL)
    }
}