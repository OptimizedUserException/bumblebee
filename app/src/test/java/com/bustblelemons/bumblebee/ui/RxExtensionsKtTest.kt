package com.bustblelemons.bumblebee.ui

import io.reactivex.Completable
import io.reactivex.disposables.CompositeDisposable
import org.junit.Assert.assertTrue
import org.junit.Test

class RxExtensionsKtTest {

    @Test fun shouldRegisterDisposableCorrectly() {
        // given
        val someDisposable = Completable.complete().subscribe()
        val composite = CompositeDisposable()

        // expected
        assertTrue(composite.size() == 0)

        // when
        someDisposable.register(composite)

        // then
        assertTrue(composite.size() > 0)
    }
}