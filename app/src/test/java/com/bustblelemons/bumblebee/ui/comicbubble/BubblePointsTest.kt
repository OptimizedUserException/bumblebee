package com.bustblelemons.bumblebee.ui.comicbubble

import android.graphics.Rect
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class BubblePointsTest {

    lateinit var bubblePoints: BubblePoints

    @Before fun setup() {
        bubblePoints = BubblePoints()
    }

    @Test fun shouldCreateCorrectly() {
        // given
        val ourBounds = Rect(0, 0, 300, 300)

        // when
        bubblePoints.bounds = ourBounds

        // then
        assertEquals(0, bubblePoints.topWestX)
        assertEquals(0, bubblePoints.topWestY)
        assertEquals(300, bubblePoints.bottomEastX)
        assertEquals(300, bubblePoints.bottomEastY)
    }

    @Test fun shouldHandleEmptyBoundsCorrectly() {
        // given
        val ourBounds = Rect(0, 0, 0, 0)

        // when
        bubblePoints.bounds = ourBounds

        // then
        assertEquals(0, bubblePoints.topWestX)
        assertEquals(0, bubblePoints.topWestY)
        assertEquals(0, bubblePoints.bottomEastX)
        assertEquals(0, bubblePoints.bottomEastY)
    }

    @Test fun shouldCalculateCenterTopPointCorrectly() {
        // given
        val top = 0
        val right = 300
        val bottom = 300
        val expectedTopX = right / 2
        val expectedTopY = 0
        val ourBounds = Rect(0, top, right, bottom)
        bubblePoints.bounds = ourBounds

        // when
        val centerTop = bubblePoints.centerTop

        // then
        assertEquals(expectedTopX, centerTop.x)
        assertEquals(expectedTopY, centerTop.y)
    }

    @Test fun shouldCalculateCenterBottomCorrectly() {
        // given
        val bottom = 300
        val right = 300
        val expectedBottomX = right / 2
        val expectedBottomY = bottom
        val ourBounds = Rect(0, 0, right, bottom)
        bubblePoints.bounds = ourBounds

        // when
        val centerBottom = bubblePoints.centerBottom

        // then
        assertEquals(expectedBottomX, centerBottom.x)
        assertEquals(expectedBottomY, centerBottom.y)
    }

    @Test fun shouldCalculateCenterWestCorrectly() {
        // given
        val bottom = 300
        val right = 300
        val expectedWestX = 0
        val expectedWestY = bottom / 2
        val ourBounds = Rect(0, 0, right, bottom)
        bubblePoints.bounds = ourBounds

        // when
        val centerWest = bubblePoints.centerWest

        // then
        assertEquals(expectedWestX, centerWest.x)
        assertEquals(expectedWestY, centerWest.y)
    }

    @Test fun shouldCalculateCenterEastCorrectly() {
        // given
        val bottom = 300
        val right = 300
        val expectedEastX = right
        val expectedEastY = bottom / 2
        val ourBounds = Rect(0, 0, right, bottom)
        bubblePoints.bounds = ourBounds

        // when
        val centerEast = bubblePoints.centerEast

        // then
        assertEquals(expectedEastX, centerEast.x)
        assertEquals(expectedEastY, centerEast.y)
    }
}