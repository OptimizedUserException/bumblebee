package com.bustblelemons.bumblebee.ui.views

import android.graphics.Rect
import android.view.View
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import java.util.*

@RunWith(RobolectricTestRunner::class)
class ViewMeasureExtensionKtTest {


    @Test fun shouldReportEachBounds() {
        // given
        val someView = View(RuntimeEnvironment.application)
        val callback = BoundsCaptor()
        someView.onEachBoundsKnown(callback)
        val random = Random()

        // when
        (0 until 25).forEach { _ ->
            val left = random.nextInt()
            val top = random.nextInt()
            val right = random.nextInt()
            val bottom = right * 2
            val expectedBounds = Rect(left, top, right, bottom)

            // when
            someView.layout(left, top, right, bottom)

            // then
            assertEquals(callback.capture.view, someView)
            assertEquals(callback.capture.bounds, expectedBounds)
        }
    }

    class BoundsCaptor : ((View, Rect) -> Unit) {

        class Capture(val view: View, val bounds: Rect)

        var capture = Capture(View(RuntimeEnvironment.application), Rect())

        override fun invoke(p1: View, p2: Rect) {
            capture = Capture(p1, p2)
        }
    }

    @Test fun shouldReturnListenerInstanceWhenCalled() {
        // given
        val someView = View(RuntimeEnvironment.application)
        val callback: ((View, Rect) -> Unit) = { _, _ -> /* empty */ }

        // when
        val listener = someView.onEachBoundsKnown(callback)

        // then
        assertNotNull(listener)
    }
}