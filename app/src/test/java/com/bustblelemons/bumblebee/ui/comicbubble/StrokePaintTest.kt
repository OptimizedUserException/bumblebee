package com.bustblelemons.bumblebee.ui.comicbubble

import android.graphics.Paint
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class StrokePaintTest {

    @Test fun shouldInitCorrectly() {
        // when
        val paint = StrokePaint()

        // then
        assertEquals(paint.color, StrokePaint.DEFAULT_COLOR)
        assertEquals(paint.strokeWidth, StrokePaint.DEFAULT_WIDTH)
        assertEquals(paint.style, Paint.Style.STROKE)
    }
}