package com.bustblelemons.bumblebee.domain.model.functions.arrow

import android.graphics.Rect
import com.bustblelemons.bumblebee.domain.model.Position
import junit.framework.Assert.assertFalse
import junit.framework.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class ArrowPointForAttachmentTest {

    private val testedFunction = ArrowPointForAttachment()

    @Test fun shouldReturnPointWithinTotalBounds() {
        // given
        val totalBounds = Rect(0, 0, 100, 100)
        val face = Rect(120, 120, 150, 150)
        val attachment = Position.Right

        // when
        val actualPoint = testedFunction(attachment, totalBounds, face)

        // then
        assertTrue(totalBounds.containsOrOnBorder(actualPoint.x, actualPoint.y))
    }

    private inline fun Rect.containsOrOnBorder(x: Int, y: Int): Boolean {
        return this.contains(x, y)
                .or(x == this.right && y <= this.bottom)
                .or(y == this.bottom && x <= this.right)
    }

    @Test fun shouldReturnPointNotWithinFaceBounds() {
        // given
        val totalBounds = Rect(0, 0, 100, 100)
        val face = Rect(120, 120, 150, 150)
        val attachment = Position.Right

        // when
        val actualPoint = testedFunction(attachment, totalBounds, face)

        // then
        assertFalse(face.contains(actualPoint.x, actualPoint.y))
    }

    @Test fun shouldReturnPointVisibleOnScreen() {
        // given
        val totalBounds = Rect(0, 0, 100, 100)
        val face = Rect(120, 120, 150, 150)
        val attachment = Position.Right

        // when
        val actualPoint = testedFunction(attachment, totalBounds, face)

        // then
        assertTrue(actualPoint.x >= 0)
        assertTrue(actualPoint.y >= 0)
    }
}