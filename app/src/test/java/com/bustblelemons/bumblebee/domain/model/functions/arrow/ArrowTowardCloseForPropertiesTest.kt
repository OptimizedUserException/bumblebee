package com.bustblelemons.bumblebee.domain.model.functions.arrow

import android.graphics.Point
import com.bustblelemons.bumblebee.domain.model.ArrowProperties
import com.bustblelemons.bumblebee.domain.model.Position
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class ArrowTowardCloseForPropertiesTest {

    private val arrowTowardCloseForProperties = ArrowTowardCloseForProperties()

    @Test fun shouldCalculateForLeftAttachmentPointTop() {
        //given
        val end = Point(60, 120)
        val point = Point(20, 20)
        val properties = ArrowProperties(Position.Left, Position.Top)
        val expected = Point(point.x, end.y)

        // when
        val actualPoint = arrowTowardCloseForProperties(properties, end, point)

        // then
        assertEquals(expected.x, actualPoint.x)
        assertEquals(expected.y, actualPoint.y)
    }

    @Test fun shouldCalculateForLeftAttachmentPointBottom() {
        //given
        val end = Point(60, 120)
        val point = Point(20, 20)
        val properties = ArrowProperties(Position.Left, Position.Bottom)
        val expected = Point(40, 120)

        // when
        val actualPoint = arrowTowardCloseForProperties(properties, end, point)

        // then
        assertEquals(expected.x, actualPoint.x)
        assertEquals(expected.y, actualPoint.y)
    }

    @Test fun shouldCalculateForTopAttachmentPointLeft() {
        //given
        val end = Point(50, 100)
        val point = Point(0, 0)
        val properties = ArrowProperties(Position.Top, Position.Left)
        val expected = Point(end.x, point.y)

        // when
        val actualPoint = arrowTowardCloseForProperties(properties, end, point)

        // then
        assertEquals(expected.x, actualPoint.x)
        assertEquals(expected.y, actualPoint.y)
    }

    @Test fun shouldCalculateForTopAttachmentPointRight() {
        //given
        val end = Point(50, 100)
        val point = Point(100, 50)
        val properties = ArrowProperties(Position.Top, Position.Right)
        val expected = Point(end.x, 75)

        // when
        val actualPoint = arrowTowardCloseForProperties(properties, end, point)

        // then
        assertEquals(expected.x, actualPoint.x)
        assertEquals(expected.y, actualPoint.y)
    }

    @Test fun shouldCalculateForRightAttachmentPointTop() {
        //given
        val end = Point(50, 100)
        val point = Point(0, 200)
        val properties = ArrowProperties(Position.Bottom, Position.Left)
        val expected = Point(end.x, point.y)

        // when
        val actualPoint = arrowTowardCloseForProperties(properties, end, point)

        // then
        assertEquals(expected.x, actualPoint.x)
        assertEquals(expected.y, actualPoint.y)
    }

    @Test fun shouldCalculateForRightAttachmentPointBottom() {
        //given
        val end = Point(50, 100)
        val point = Point(0, 200)
        val properties = ArrowProperties(Position.Bottom, Position.Left)
        val expected = Point(end.x, point.y)

        // when
        val actualPoint = arrowTowardCloseForProperties(properties, end, point)

        // then
        assertEquals(expected.x, actualPoint.x)
        assertEquals(expected.y, actualPoint.y)
    }

    @Test fun shouldCalculateForBottomAttachmentPointLeft() {
        //given
        val end = Point(50, 100)
        val point = Point(0, 200)
        val properties = ArrowProperties(Position.Bottom, Position.Left)
        val expected = Point(50, 200)

        // when
        val actualPoint = arrowTowardCloseForProperties(properties, end, point)

        // then
        assertEquals(expected.x, actualPoint.x)
        assertEquals(expected.y, actualPoint.y)
    }

    @Test fun shouldCalculateForBottomAttachmentPointRight() {
        //given
        val end = Point(50, 100)
        val point = Point(200, 200)
        val properties = ArrowProperties(Position.Bottom, Position.Right)
        val expected = Point(50, 150)

        // when
        val actualPoint = arrowTowardCloseForProperties(properties, end, point)

        // then
        assertEquals(expected.x, actualPoint.x)
        assertEquals(expected.y, actualPoint.y)
    }
}