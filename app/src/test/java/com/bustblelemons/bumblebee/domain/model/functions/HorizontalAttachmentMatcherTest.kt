package com.bustblelemons.bumblebee.domain.model.functions

import android.graphics.Rect
import com.bustblelemons.bumblebee.domain.model.Position
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class HorizontalAttachmentMatcherTest {

    private val testCenterPosition = Position.Left

    private val matcher = object : HorizontalAttachmentMatcher() {
        override val centerPosition: Position
            get() = testCenterPosition
    }

    @Test fun shouldReturnTopPositionCorrectly() {
        // given
        val face = Rect(110, 0, 140, 20)
        val bubble = Rect(0, 0, 100, 100)
        val expectedPosition = Position.Top

        // when
        val actualPosition = matcher(face, bubble)

        // then
        assertEquals(expectedPosition, actualPosition)
    }

    @Test fun shouldReturnBottomPositionCorrectly() {
        // given
        val bubble = Rect(0, 0, 100, 100)
        val face = Rect(110, 80, 140, 100)
        val expectedPosition = Position.Bottom

        // when
        val actualPosition = matcher(face, bubble)

        // then
        assertEquals(expectedPosition, actualPosition)
    }

    @Test fun shouldReturnCenterPositionCorrectly() {
        // given
        val face = Rect(110, 40, 140, 60)
        val bubble = Rect(0, 0, 100, 100)

        // when
        val actualPosition = matcher(face, bubble)

        // then
        assertEquals(testCenterPosition, actualPosition)
    }
}