package com.bustblelemons.bumblebee.domain.model.functions.arrow

import android.graphics.Point
import android.graphics.Rect
import com.bustblelemons.bumblebee.domain.ArrowPropertiesFactory
import com.bustblelemons.bumblebee.domain.model.Position
import com.bustblelemons.bumblebee.ui.comicbubble.BubblePoints
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class ArrowEndForPropertiesTest {

    private val arrowEndForProperties = ArrowEndForProperties()

    @Test fun shouldCalculateEndPointForAttachmentLeftCorrectly() {
        // given
        val properties = ArrowPropertiesFactory.createProperties(Position.Left)
        val bubblePoints = bubblePoints()
        val expectedPoint = Point(0, 55)

        // when
        val actualPoint = arrowEndForProperties(properties, bubblePoints)

        // then
        assertEquals(expectedPoint.x, actualPoint.x)
        assertEquals(expectedPoint.y, actualPoint.y)
    }

    @Test fun shouldCalculateEndPointForAttachmentRightCorrectly() {
        // given
        val properties = ArrowPropertiesFactory.createProperties(Position.Right)
        val bubblePoints = bubblePoints()
        val expectedPoint = Point(bubblePoints.topEast.x, 55)

        // when
        val actualPoint = arrowEndForProperties(properties, bubblePoints)

        // then
        assertEquals(expectedPoint.x, actualPoint.x)
        assertEquals(expectedPoint.y, actualPoint.y)
    }

    @Test fun shouldCalculateEndPointForAttachmentTopCorrectly() {
        // given
        val properties = ArrowPropertiesFactory.createProperties(Position.Top)
        val bubblePoints = bubblePoints()
        val expectedPoint = Point(55, 0)

        // when
        val actualPoint = arrowEndForProperties(properties, bubblePoints)

        // then
        assertEquals(expectedPoint.x, actualPoint.x)
        assertEquals(expectedPoint.y, actualPoint.y)
    }

    @Test fun shouldCalculateEndPointForAttachmentBottomCorrectly() {
        // given
        val properties = ArrowPropertiesFactory.createProperties(Position.Bottom)
        val bubblePoints = bubblePoints()
        val expectedPoint = Point(55, 100)

        // when
        val actualPoint = arrowEndForProperties(properties, bubblePoints)

        // then
        assertEquals(expectedPoint.x, actualPoint.x)
        assertEquals(expectedPoint.y, actualPoint.y)
    }

    private fun bubblePoints() = BubblePoints().also { it.bounds = Rect(0, 0, 100, 100) }

}