package com.bustblelemons.bumblebee.domain.model.functions

import android.graphics.Rect
import com.bustblelemons.bumblebee.anyObject
import com.bustblelemons.bumblebee.domain.model.Position
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class PointsTowardPositionTest {

    @Mock lateinit var leftAttachmentMatcher: LeftAttachmentMatcher
    @Mock lateinit var topAttachmentMatcher: TopAttachmentMatcher
    @Mock lateinit var rightAttachmentMatcher: RightAttachmentMatcher
    @Mock lateinit var bottomAttachmentMatcher: BottomAttachmentMatcher

    private lateinit var pointsTowardPosition: PointsTowardPosition

    @Before fun setup() {
        MockitoAnnotations.initMocks(this)
        pointsTowardPosition = PointsTowardPosition(
                leftAttachmentMatcher,
                topAttachmentMatcher,
                rightAttachmentMatcher,
                bottomAttachmentMatcher
        )
    }

    @Test fun shouldReturnPositionForAttachedLeftCorrectly() {
        // given
        val face = Rect(0, 0, 20, 20)
        val bubble = Rect(20, 0, 100, 100)
        val expectedPosition = Position.Left
        val attachment = Position.Left
        given(leftAttachmentMatcher.invoke(anyObject(), anyObject()))
                .willReturn(expectedPosition)

        // when
        val actualPosition = pointsTowardPosition(attachment, face, bubble)

        // then
        assertEquals(expectedPosition, actualPosition)
    }

    @Test fun shouldReturnPositionForAttachedRightCorrectly() {
        // given
        val face = Rect(0, 50, 20, 100)
        val bubble = Rect(20, 0, 100, 100)
        val expectedPosition = Position.Right
        val attachment = Position.Right
        given(rightAttachmentMatcher.invoke(anyObject(), anyObject()))
                .willReturn(expectedPosition)

        // when
        val actualPosition = pointsTowardPosition(attachment, face, bubble)

        // then
        assertEquals(expectedPosition, actualPosition)
    }

    @Test fun shouldReturnPositionForAttachedTopCorrectly() {
        // given
        val face = Rect(0, 50, 20, 60)
        val bubble = Rect(20, 0, 100, 100)
        val attachment = Position.Top
        val expectedPosition = Position.Top
        given(topAttachmentMatcher(anyObject(), anyObject()))
                .willReturn(expectedPosition)

        // when
        val actualPosition = pointsTowardPosition(attachment, face, bubble)

        // then
        assertEquals(expectedPosition, actualPosition)
    }

    @Test fun shouldReturnPositionForAttachedBottomCorrectly() {
        // given
        val face = Rect(0, 50, 20, 60)
        val bubble = Rect(20, 0, 100, 100)
        val expectedPosition = Position.Bottom
        val attachment = Position.Bottom
        given(bottomAttachmentMatcher(anyObject(), anyObject()))
                .willReturn(expectedPosition)

        // when
        val actualPosition = pointsTowardPosition(attachment, face, bubble)

        // then
        assertEquals(expectedPosition, actualPosition)
    }

    @Test fun shouldReturnPositionForAttachedNoneCorrectly() {
        // given
        val face = Rect(0, 50, 20, 60)
        val bubble = Rect(20, 0, 100, 100)
        val expectedPosition = Position.Bottom
        val attachment = Position.None
        given(bottomAttachmentMatcher(anyObject(), anyObject()))
                .willReturn(expectedPosition)

        // when
        val actualPosition = pointsTowardPosition(attachment, face, bubble)

        // then
        assertEquals(expectedPosition, actualPosition)
    }
}