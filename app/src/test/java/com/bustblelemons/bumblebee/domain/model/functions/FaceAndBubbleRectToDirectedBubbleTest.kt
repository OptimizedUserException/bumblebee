package com.bustblelemons.bumblebee.domain.model.functions

import android.graphics.Point
import android.graphics.Rect
import com.bustblelemons.bumblebee.ComicBubblesFactory
import com.bustblelemons.bumblebee.anyObject
import com.bustblelemons.bumblebee.domain.ArrowPropertiesFactory
import com.bustblelemons.bumblebee.domain.model.BubbleWithDirection
import com.bustblelemons.bumblebee.domain.model.Position
import com.bustblelemons.bumblebee.domain.model.functions.arrow.BubbleArrowFromProperties
import com.bustblelemons.bumblebee.domain.model.functions.arrow.CurvesForArrowProperties
import com.bustblelemons.bumblebee.ui.comicbubble.BubbleArrow
import com.bustblelemons.bumblebee.ui.comicbubble.BubblePoints
import com.bustblelemons.bumblebee.ui.comicbubble.ComicBubble
import com.bustblelemons.bumblebee.ui.comicbubble.math.QuadraticBezierCurve
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnit
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class FaceAndBubbleRectToDirectedBubbleTest {

    @get:Rule val rule = MockitoJUnit.rule()

    @InjectMocks lateinit var faceAndBubbleRectToDirectedBubble: FaceAndBubbleRectToDirectedBubble
    @Mock lateinit var rectsToComicBubble: RectToComicBubble
    @Mock lateinit var arrowPropertiesForFace: ArrowPropertiesForFace
    @Mock lateinit var curvesForArrowProperties: CurvesForArrowProperties
    @Mock lateinit var bubblePointsFromProperties: BubblePointsFromProperties
    @Mock lateinit var bubbleArrowFromProperties: BubbleArrowFromProperties

    private val testProperties = ArrowPropertiesFactory.createProperties(Position.None)
    private val testCurves = QuadraticBezierCurveFactory.someCurves()
    private val testBubblePoints = BubblePoints()
    private val testFace = Rect()
    private val testBubbleBounds = Rect()
    private val testComicBubble = ComicBubblesFactory.oneBubble
    private val testBubbleArrow = BubbleArrow(Point(), Point(), Point(), Point(), Point())


    @Before fun setup() {
        given(rectsToComicBubble.invoke(anyObject()))
                .willReturn(testComicBubble)
        given(arrowPropertiesForFace(anyObject(), anyObject()))
                .willReturn(testProperties)
        given(curvesForArrowProperties(anyObject(), anyObject(), anyObject()))
                .willReturn(testCurves)
        given(bubblePointsFromProperties(anyObject(), anyObject()))
                .willReturn(testBubblePoints)
        given(bubbleArrowFromProperties(anyObject(), anyObject(), anyObject(), anyObject()))
                .willReturn(testBubbleArrow)
    }

    @Test fun shouldReturnBubbleWithDirectionCorrectly() {
        // given
        val expectBubble = createExpectedBubble(testComicBubble, testFace)

        // when
        val actualBubble = faceAndBubbleRectToDirectedBubble(testFace, testBubbleBounds)

        // then
        assertEquals(expectBubble.bubble, actualBubble.bubble)
        assertEquals(expectBubble.face, actualBubble.face)
        assertEquals(expectBubble.curves, actualBubble.curves)
    }

    private fun createExpectedBubble(comicBubble: ComicBubble, face: Rect): BubbleWithDirection {
        return BubbleWithDirection(comicBubble, face)
                .apply { curves = testCurves }
    }

    object QuadraticBezierCurveFactory {
        fun someCurves(): Array<QuadraticBezierCurve> {
            return (1..10)
                    .map { index -> singleCurve(index) }
                    .toTypedArray()
        }

        private fun singleCurve(index: Int): QuadraticBezierCurve {
            return QuadraticBezierCurve(
                    Point(index.times(1), index.times(1)),
                    Point(index.times(2), index.times(2)),
                    Point(index.times(3), index.times(3))
            )
        }
    }
}