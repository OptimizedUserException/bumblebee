package com.bustblelemons.bumblebee.domain.model.functions.arrow

import android.graphics.Rect
import com.bustblelemons.bumblebee.domain.model.ArrowProperties
import com.bustblelemons.bumblebee.domain.model.Position
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class ArrowBoundsFromPropertiesTest {

    private val boundsFromProperties = ArrowBoundsFromProperties()

    @Test fun shouldReturnProperBoundsForLeftAttachment() {
        // given
        val attachment = Position.Left
        val totalBounds = Rect(0, 0, 100, 100)
        val bubbleBounds = Rect(20, 0, 100, 100)
        val expectedArrowBounds = Rect(0, 0, 20, 100)

        // when
        val actualArrowBounds = boundsFromProperties(attachment, totalBounds, bubbleBounds)

        // then
        assertEquals(expectedArrowBounds, actualArrowBounds)
    }

    @Test fun shouldReturnProperBoundsForTopAttachment() {
        // given
        val attachment = Position.Top
        val totalBounds = Rect(0, 0, 100, 100)
        val bubbleBounds = Rect(0, 20, 100, 100)
        val expectedArrowBounds = Rect(0, 0, 100, 20)

        // when
        val actualArrowBounds = boundsFromProperties(attachment, totalBounds, bubbleBounds)

        // then
        assertEquals(expectedArrowBounds, actualArrowBounds)
    }

    @Test fun shouldReturnProperBoundsForRightAttachment() {
        // given
        val attachment = Position.Right
        val totalBounds = Rect(0, 0, 100, 100)
        val bubbleBounds = Rect(0, 0, 80, 100)
        val expectedArrowBounds = Rect(80, 0, 100, 100)

        // when
        val actualArrowBounds = boundsFromProperties(attachment, totalBounds, bubbleBounds)

        // then
        assertEquals(expectedArrowBounds, actualArrowBounds)
    }

    @Test fun shouldReturnProperBoundsForBottomAttachment() {
        // given
        val attachment = Position.Bottom
        val totalBounds = Rect(0, 0, 100, 100)
        val bubbleBounds = Rect(0, 0, 100, 80)
        val expectedArrowBounds = Rect(0, 80, 100, 100)

        // when
        val actualArrowBounds = boundsFromProperties(attachment, totalBounds, bubbleBounds)

        // then
        assertEquals(expectedArrowBounds, actualArrowBounds)
    }
}