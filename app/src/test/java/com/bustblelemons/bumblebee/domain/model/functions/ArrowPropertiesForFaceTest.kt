package com.bustblelemons.bumblebee.domain.model.functions

import android.graphics.Rect
import com.bustblelemons.bumblebee.anyObject
import com.bustblelemons.bumblebee.domain.model.ArrowProperties
import com.bustblelemons.bumblebee.domain.model.Position
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class ArrowPropertiesForFaceTest {

    @Test fun shouldReturnPropertiesForFaceOnTheLeftOfBubble() {
        // given
        val face = Rect(0, 0, 10, 10)
        val bubble = Rect(20, 0, 100, 100)
        val pointsTowardPosition = mock(PointsTowardPosition::class.java)
        val expectedArrowDirection = Position.Left
        given(pointsTowardPosition.invoke(anyObject(), anyObject(), anyObject()))
                .willReturn(expectedArrowDirection)
        val testedFunction = ArrowPropertiesForFace(pointsTowardPosition)
        val expectedProperties = ArrowProperties(expectedArrowDirection, expectedArrowDirection)

        // when
        val actualArrowProperties = testedFunction(face, bubble)

        // then
        assertEquals(expectedProperties, actualArrowProperties)
    }

    @Test fun shouldReturnPropertiesForFaceAboveOfBubble() {
        // given
        val face = Rect(10, 0, 20, 10)
        val bubble = Rect(10, 20, 100, 100)
        val pointsTowardPosition = mock(PointsTowardPosition::class.java)
        val expectedArrowDirection = Position.Top
        given(pointsTowardPosition.invoke(anyObject(), anyObject(), anyObject()))
                .willReturn(expectedArrowDirection)
        val testedFunction = ArrowPropertiesForFace(pointsTowardPosition)
        val expectedProperties = ArrowProperties(expectedArrowDirection, expectedArrowDirection)

        // when
        val actualArrowProperties = testedFunction(face, bubble)

        // then
        assertEquals(expectedProperties, actualArrowProperties)
    }

    @Test fun shouldReturnPropertiesForFaceOnTheRightOfBubble() {
        // given
        val face = Rect(20, 0, 40, 10)
        val bubble = Rect(0, 0, 10, 100)
        val pointsTowardPosition = mock(PointsTowardPosition::class.java)
        val expectedArrowDirection = Position.Right
        given(pointsTowardPosition.invoke(anyObject(), anyObject(), anyObject()))
                .willReturn(expectedArrowDirection)
        val testedFunction = ArrowPropertiesForFace(pointsTowardPosition)
        val expectedProperties = ArrowProperties(expectedArrowDirection, expectedArrowDirection)

        // when
        val actualArrowProperties = testedFunction(face, bubble)

        // then
        assertEquals(expectedProperties, actualArrowProperties)
    }

    @Test fun shouldReturnPropertiesForFaceBelowOfBubble() {
        // given
        val face = Rect(0, 50, 10, 100)
        val bubble = Rect(0, 10, 100, 40)
        val pointsTowardPosition = mock(PointsTowardPosition::class.java)
        val expectedArrowDirection = Position.Bottom
        given(pointsTowardPosition.invoke(anyObject(), anyObject(), anyObject()))
                .willReturn(expectedArrowDirection)
        val testedFunction = ArrowPropertiesForFace(pointsTowardPosition)
        val expectedProperties = ArrowProperties(expectedArrowDirection, expectedArrowDirection)

        // when
        val actualArrowProperties = testedFunction(face, bubble)

        // then
        assertEquals(expectedProperties, actualArrowProperties)
    }
}