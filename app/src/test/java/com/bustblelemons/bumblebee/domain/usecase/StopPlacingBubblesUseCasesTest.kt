package com.bustblelemons.bumblebee.domain.usecase

import com.bustblelemons.bumblebee.repository.facesdetection.FacesDetectedRepository
import org.junit.Test
import org.mockito.Mockito.mock

class StopPlacingBubblesUseCasesTest {

    @Test fun shouldStopPlacingBubblesCorrectly() {
        // given
        val facesDetectedRepository = mock(FacesDetectedRepository::class.java)
        val usecase = StopPlacingBubblesUseCases(TestRxSchedulers(), facesDetectedRepository)

        // when
        val test = usecase.rxStream.test()

        // then
        test.assertComplete()
        test.assertNoErrors()
    }
}