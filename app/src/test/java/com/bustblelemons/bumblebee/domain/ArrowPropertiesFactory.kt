package com.bustblelemons.bumblebee.domain

import com.bustblelemons.bumblebee.domain.model.ArrowProperties
import com.bustblelemons.bumblebee.domain.model.Position

object ArrowPropertiesFactory {

    fun createProperties(attached: Position): ArrowProperties {
        return ArrowProperties(
                attached, attached,
                TEST_ARROW_SIZE,
                TEST_ARROW_OPEN_SIZE
        )
    }

    const val TEST_ARROW_SIZE = 0.25F
    const val TEST_ARROW_OPEN_SIZE = 0.1F
}