package com.bustblelemons.bumblebee.domain.model

import android.graphics.Rect
import com.bustblelemons.bumblebee.domain.containsAtLeast
import com.bustblelemons.bumblebee.domain.containsCenterOf
import com.bustblelemons.bumblebee.domain.containsCornerOf
import com.bustblelemons.bumblebee.domain.field
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class RectExtensionsKtTest {

    @Test
    fun shouldFieldCorrectly() {
        // given
        val bottom = 10
        val right = 20
        val expectedField = right * bottom
        val rect = Rect(0, 0, right, bottom)

        // when
        val actualField = rect.field

        // then
        assertEquals(actualField, expectedField)
    }

    @Test
    fun shouldReportZeroForNullObject() {
        // when
        val field = (null as Rect?).field

        // then
        assertEquals(0, field)
    }

    @Test
    fun shouldReportDoesNotContainsCorrectly() {
        // given
        val left = 10
        val top = 10
        val right = 100
        val bottom = 100
        val rect = Rect(left, top, right, bottom)
        val notContainLeft = right
        val notContainTop = bottom
        val notContainRight = notContainLeft + 10
        val notContainBottom = notContainTop + 10
        val percentage = 10
        val shouldNotContain = Rect(notContainLeft, notContainTop, notContainRight, notContainBottom)

        // when
        val shouldBeFalse = rect.containsAtLeast(shouldNotContain, percentage)

        // then
        assertFalse(shouldBeFalse)
    }

    @Test
    fun shouldReportContainsTopLeftCornerOfCorrectly() {
        // given
        val testedRect = Rect(10, 10, 100, 100)
        val hasCornerWithTestedRect = Rect(15, 15, 150, 150)

        // when
        val actual = testedRect.containsCornerOf(hasCornerWithTestedRect)

        // then
        assertTrue(actual)
    }

    @Test
    fun shouldReportContainsTopRightCornerOfCorrectly() {
        // given
        val testedRect = Rect(10, 10, 100, 100)
        val hasCornerWithTestedRect = Rect(5, 10, 99, 150)

        // when
        val actual = testedRect.containsCornerOf(hasCornerWithTestedRect)

        // then
        assertTrue(actual)
    }

    @Test
    fun shouldReportContainsBottomLeftCornerOfCorrectly() {
        // given
        val testedRect = Rect(10, 10, 100, 100)
        val hasCornerWithTestedRect = Rect(11, 5, 150, 99)

        // when
        val actual = testedRect.containsCornerOf(hasCornerWithTestedRect)

        // then
        assertTrue(actual)
    }

    @Test
    fun shouldReportContainsBottomRightCornerOfCorrectly() {
        // given
        val testedRect = Rect(10, 10, 100, 100)
        val hasCornerWithTestedRect = Rect(5, 5, 99, 99)

        // when
        val actual = testedRect.containsCornerOf(hasCornerWithTestedRect)

        // then
        assertTrue(actual)
    }

    @Test
    fun shouldReportContainNoCornerOfCorrectly() {
        // given
        val testedRect = Rect(100, 100, 200, 200)
        val hasCornerWithTestedRect = Rect(50, 50, 300, 300)

        // when
        val actual = testedRect.containsCornerOf(hasCornerWithTestedRect)

        // then
        assertFalse(actual)
    }

    @Test
    fun shouldReportContainsAtLeastCorrectly() {
        // given
        val testedRect = Rect(100, 100, 200, 200)
        val requiredPercentage = 45.0
        val testedField = testedRect.field
        val requiredField = (testedField.toDouble() * (requiredPercentage / 100.0)).toInt()
        val shouldBeWithinPercentage = Rect(100, 100, 150, 190)

        // when
        val actual = testedRect.containsAtLeast(shouldBeWithinPercentage, requiredPercentage.toInt())

        // then
        assertTrue(actual)
    }

    @Test fun shouldReportContainsCenterOfCorrectly() {
        // given
        val anotherRect = Rect(0, 0, 10, 10)
        val rect = Rect(0, 0, 100, 100)

        // when
        val actual = rect.containsCenterOf(anotherRect)

        // then
        assertTrue(actual)
    }

    @Test fun shouldReportNotContainsCenterOfCorrectly() {
        // given
        val anotherRect = Rect(0, 0, 100, 100)
        val rect = Rect(0, 0, 10, 10)

        // when
        val actual = rect.containsCenterOf(anotherRect)

        // then
        assertFalse(actual)
    }
}