package com.bustblelemons.bumblebee.domain.model.functions

import android.graphics.Point
import android.graphics.Rect
import com.bustblelemons.bumblebee.anyObject
import com.bustblelemons.bumblebee.domain.ArrowPropertiesFactory
import com.bustblelemons.bumblebee.domain.model.Position
import com.bustblelemons.bumblebee.domain.model.functions.arrow.*
import com.bustblelemons.bumblebee.ui.comicbubble.BubblePoints
import com.bustblelemons.bumblebee.ui.comicbubble.math.QuadraticBezierCurve
import junit.framework.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnit
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class BubbleArrowFromPropertiesTest {

    @get:Rule val rule = MockitoJUnit.rule()

    @InjectMocks lateinit var function: BubbleArrowFromProperties
    @Mock lateinit var towardCloseForProperties: ArrowTowardCloseForProperties
    @Mock lateinit var towardOpenForProperties: ArrowTowardOpenForProperties
    @Mock lateinit var arrowEndForProperties: ArrowEndForProperties
    @Mock lateinit var arrowPointForProperties: ArrowPointForAttachment
    @Mock lateinit var arrowStartForProperties: ArrowStartForProperties

    @Test fun shouldCreateBubbleArrowCorrectly() {
        // given
        val bubblePoint = BubblePoints()
        val properties = ArrowPropertiesFactory.createProperties(Position.Left)
        val totalBounds = Rect()
        val face = Rect()
        val expectedStart = Point(100, 100)
        val expectedEnd = Point(101, 102)
        val expectedPoint = Point(102, 102)
        val someOpenToward = Point(103, 103)
        val someCloseToward = Point(104, 104)
        val expectedCurveOpen = QuadraticBezierCurve(expectedStart, someOpenToward, expectedPoint)
        val expectedCurveClose = QuadraticBezierCurve(expectedPoint, someCloseToward, expectedEnd)
        setupArrowProperties(expectedStart, expectedPoint, expectedEnd)
        setupOpenCloseTowards(someOpenToward, someCloseToward)

        // when
        val actualArrow = function(bubblePoint, properties, totalBounds, face)

        // then
        assertEquals(expectedStart, actualArrow.start)
        assertEquals(expectedPoint, actualArrow.point)
        assertEquals(expectedEnd, actualArrow.end)
        assertEquals(expectedCurveOpen, actualArrow.curveOpen)
        assertEquals(expectedCurveClose, actualArrow.curveClose)

    }

    private fun setupOpenCloseTowards(someOpenToward: Point, someCloseToward: Point) {
        given(towardOpenForProperties(anyObject(), anyObject(), anyObject()))
                .willReturn(someOpenToward)
        given(towardCloseForProperties(anyObject(), anyObject(), anyObject()))
                .willReturn(someCloseToward)
    }

    private fun setupArrowProperties(expectedStart: Point, expectedPoint: Point, expectedEnd: Point) {
        given(arrowStartForProperties(anyObject(), anyObject()))
                .willReturn(expectedStart)
        given(arrowPointForProperties(anyObject(), anyObject(), anyObject()))
                .willReturn(expectedPoint)
        given(arrowEndForProperties(anyObject(), anyObject()))
                .willReturn(expectedEnd)
    }
}