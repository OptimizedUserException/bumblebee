package com.bustblelemons.bumblebee.domain.model.functions

import android.graphics.Rect
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class) class RectToComicBubbleTest {

    @Test fun shouldMapCorrectly() {
        // given
        val function = RectToComicBubble()
        val someRect = Rect()

        // when
        val actual = function.invoke(someRect)

        // then
        assertEquals(actual.bounds, someRect)
    }
}