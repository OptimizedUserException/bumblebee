package com.bustblelemons.bumblebee.domain.model.functions

import android.graphics.Rect
import com.bustblelemons.bumblebee.domain.ArrowPropertiesFactory
import com.bustblelemons.bumblebee.domain.model.Position
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class BubblePointsFromPropertiesTest {

    private val arrowSizePart = 0.25F
    private val function = BubblePointsFromProperties()

    @Test fun shouldCalculateForLeftAttachmentCorrectly() {
        // given
        val bounds = Rect(100, 0, 200, 200)
        val expectedBounds = Rect(125, 0, 200, 200)
        val properties = ArrowPropertiesFactory.createProperties(Position.Left)

        // when
        val points = function(properties, bounds)

        // then
        assertEquals(expectedBounds, points.bounds)
    }

    @Test fun shouldCalculateForTopAttachmentCorrectly() {
        // given
        val bounds = Rect(0, 0, 100, 100)
        val expectedBounds = Rect(0, 25, 100, 100)
        val properties = ArrowPropertiesFactory.createProperties(Position.Top)

        // when
        val points = function(properties, bounds)

        // then
        assertEquals(expectedBounds, points.bounds)
    }

    @Test fun shouldCalculateForRightAttachmentCorrectly() {
        // given
        val bounds = Rect(100, 0, 200, 200)
        val expectedBounds = Rect(100, 0, 175, 200)
        val properties = ArrowPropertiesFactory.createProperties(Position.Right)

        // when
        val points = function(properties, bounds)

        // then
        assertEquals(expectedBounds, points.bounds)
    }

    @Test fun shouldCalculateForBottomAttachmentCorrectly() {
        // given
        val bounds = Rect(0, 0, 100, 100)
        val expectedBounds = Rect(0, 0, 100, 75)
        val properties = ArrowPropertiesFactory.createProperties(Position.Bottom)

        // when
        val points = function(properties, bounds)

        // then
        assertEquals(expectedBounds, points.bounds)
    }
}