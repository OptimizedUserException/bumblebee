package com.bustblelemons.bumblebee.domain.model.functions

import android.graphics.Rect
import com.bustblelemons.bumblebee.ComicBubblesFactory
import com.bustblelemons.bumblebee.DirectedComicBubblesFactory
import com.bustblelemons.bumblebee.anyObject
import com.bustblelemons.bumblebee.repository.bubbles.BubblesRepository
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnit
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class FaceRectsToDirectedComicBubblesTest {

    @get:Rule val rule = MockitoJUnit.rule()
    @InjectMocks lateinit var function: FaceRectsToDirectedComicBubbles
    @Mock lateinit var comparator: RectBySize
    @Mock lateinit var faceToBubbleRects: FaceToBubbleRects
    @Mock lateinit var faceAndBubbleRectToDirectedBubble: FaceAndBubbleRectToDirectedBubble
    @Mock lateinit var bubblesRepository: BubblesRepository
    private val testBubbles = ComicBubblesFactory.someBubbles()

    @Before fun setup() {
        given(faceToBubbleRects(anyObject())).willReturn(Rect())
        given(faceAndBubbleRectToDirectedBubble(anyObject(), anyObject()))
                .willReturn(DirectedComicBubblesFactory.oneBubble)
        given(bubblesRepository.bubbles).willReturn(testBubbles)
    }

    @Test fun shouldMapCorrectly() {
        val someRects = listOf(Rect(), Rect(10, 10, 2, 20))
        val expectedSize = FaceRectsToDirectedComicBubbles.FACE_THRESHOLD_LIMIT

        // when
        val actual = function(someRects)

        // then
        assertEquals(expectedSize, actual.size)
    }
}