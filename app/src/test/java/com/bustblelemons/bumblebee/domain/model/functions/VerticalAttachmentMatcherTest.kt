package com.bustblelemons.bumblebee.domain.model.functions

import android.graphics.Rect
import com.bustblelemons.bumblebee.domain.model.Position
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class VerticalAttachmentMatcherTest {

    val testedCenterPosition = Position.Bottom

    private val matcher = object : VerticalAttachmentMatcher() {
        override val centerPosition: Position
            get() = testedCenterPosition
    }

    @Test fun shouldReturnLeftPositionCorrectly() {
        // given
        val face = Rect(0, 0, 30, 60)
        val bubble = Rect(0, 60, 100, 100)
        val expectedPosition = Position.Left

        // when
        val actualPosition = matcher(face, bubble)

        // then
        assertEquals(expectedPosition, actualPosition)
    }

    @Test fun shouldReturnRightPositionCorrectly() {
        // given
        val face = Rect(80, 0, 100, 60)
        val bubble = Rect(0, 60, 100, 100)
        val expectedPosition = Position.Right

        // when
        val actualPosition = matcher(face, bubble)

        // then
        assertEquals(expectedPosition, actualPosition)
    }

    @Test fun shouldReturnCenterPositionCorrectly() {
        // given
        val face = Rect(50, 0, 60, 60)
        val bubble = Rect(0, 60, 100, 100)

        // when
        val actualPosition = matcher(face, bubble)

        // then
        assertEquals(testedCenterPosition, actualPosition)
    }
}