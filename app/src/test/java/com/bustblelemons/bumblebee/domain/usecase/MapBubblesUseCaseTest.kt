package com.bustblelemons.bumblebee.domain.usecase

import com.bustblelemons.bumblebee.ComicBubblesFactory
import com.bustblelemons.bumblebee.DirectedComicBubblesFactory
import com.bustblelemons.bumblebee.RectanglesFactory
import com.bustblelemons.bumblebee.anyObject
import com.bustblelemons.bumblebee.domain.model.functions.FaceRectsToDirectedComicBubbles
import com.bustblelemons.bumblebee.domain.model.functions.RectangleToRectMapper
import com.bustblelemons.bumblebee.repository.facesdetection.FacesDetectedRepository
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class MapBubblesUseCaseTest {

    lateinit var usecase: MapBubblesUseCase
    val rxSchedulers: RxSchedulers = TestRxSchedulers()
    @Mock
    lateinit var repository: FacesDetectedRepository
    @Mock
    lateinit var rectangleToRectMapper: RectangleToRectMapper
    @Mock
    lateinit var faceRectsToComicBubbles: FaceRectsToDirectedComicBubbles

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        usecase = MapBubblesUseCase(
                rxSchedulers,
                repository,
                rectangleToRectMapper,
                faceRectsToComicBubbles
        )
    }

    @Test
    fun shouldMapBubblesCorrectly() {
        // given
        val someBubbles = DirectedComicBubblesFactory.someBubbles()
        val rects = RectFactory.someRects()
        val someFaces = RectanglesFactory.someFaces()
        given(repository.detectFaces).willReturn(Flowable.fromArray(someFaces.toTypedArray()))
        given(rectangleToRectMapper(anyObject())).willReturn(rects)
        given(faceRectsToComicBubbles.invoke(anyObject())).willReturn(someBubbles)

        // when
        val testSubscriber = usecase.rxStream.test()

        // then
        testSubscriber.assertValue { actual -> actual contentDeepEquals someBubbles.toTypedArray() }
    }
}