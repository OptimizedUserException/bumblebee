package com.bustblelemons.bumblebee.domain.usecase

import com.bustblelemons.bumblebee.repository.facesdetection.FacesDetectedRepository
import io.fotoapparat.view.CameraRenderer
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.mock

class StartDetectionUseCaseTest {

    val schedulers = TestRxSchedulers()
    lateinit var facesRepository: FacesDetectedRepository
    lateinit var cameraRenderer: CameraRenderer
    lateinit var usecase: StartDetectionUseCase

    @Before
    fun setup() {
        facesRepository = mock(FacesDetectedRepository::class.java)
        cameraRenderer = mock(CameraRenderer::class.java)
        usecase = StartDetectionUseCase(schedulers, facesRepository, cameraRenderer)
    }

    @Test
    fun shouldCreateRxStreamCorrectly() {
        // when
        val rxScheduledStream = usecase.rxStream.test()

        // then
        rxScheduledStream.assertComplete()
        rxScheduledStream.assertNoErrors()
    }

    @Test(expected = IllegalArgumentException::class)
    fun shouldThrowOnCameraRendererNotSet() {
        // given
        val usecase = StartDetectionUseCase(schedulers, facesRepository)

        // when
        val rxScheduledStream = usecase.rxStream.test()

        // then
        rxScheduledStream.assertFailure(IllegalStateException::class.java, *emptyArray<Void>())
    }

    @Test
    fun shouldSetCameraRendererViaBuilderCorrectly() {
        // given
        val usecase = StartDetectionUseCase(schedulers, facesRepository)
        val someRenderer = mock(CameraRenderer::class.java)

        // when
        val useCaseWithRenderer = usecase.renderer(someRenderer)

        // then
        assertNotNull(useCaseWithRenderer)
    }
}