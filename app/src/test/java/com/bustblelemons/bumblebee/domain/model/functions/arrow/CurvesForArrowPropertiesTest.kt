package com.bustblelemons.bumblebee.domain.model.functions.arrow

import android.graphics.Point
import com.bustblelemons.bumblebee.domain.model.ArrowProperties
import com.bustblelemons.bumblebee.domain.model.Position
import com.bustblelemons.bumblebee.ui.comicbubble.BubbleArrow
import com.bustblelemons.bumblebee.ui.comicbubble.BubblePoints
import com.bustblelemons.bumblebee.ui.comicbubble.math.QuadraticBezierCurve
import org.junit.Assert
import org.junit.Assert.assertArrayEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class CurvesForArrowPropertiesTest {

    private val someCurveClose = quadraticCurves(1)
    private val someCurveOpen = quadraticCurves(2)

    private val someTopLeftCurve = quadraticCurves(3)
    private val someTopLeftCurveClockwise = quadraticCurves(4)
    private val someTopRightCurve = quadraticCurves(5)
    private val someTopRightCurveClockwise = quadraticCurves(6)
    private val someBottomLeftCurve = quadraticCurves(7)
    private val someBottomLeftCurveClockWise = quadraticCurves(8)
    private val someBottomRightCurve = quadraticCurves(9)
    private val someBottomRightCurveClockwise = quadraticCurves(10)

    private fun quadraticCurves(point: Int) = QuadraticBezierCurve(point(point), point(point), point(point))

    private fun point(point: Int) = Point(point, point)

    private val bubbleArrow = mock(BubbleArrow::class.java).apply {
        given(curveOpen).willReturn(someCurveOpen)
        given(curveClose).willReturn(someCurveClose)
    }

    private val bubblePoints = mock(BubblePoints::class.java).apply {
        given(topLeftCurve).willReturn(someTopLeftCurve)
        given(topLeftCurveClockWise).willReturn(someTopLeftCurveClockwise)

        given(topRightCurve).willReturn(someTopRightCurve)
        given(topRightCurveClockWise).willReturn(someTopRightCurveClockwise)

        given(bottomRightCurve).willReturn(someBottomRightCurve)
        given(bottomRightCurveClockWise).willReturn(someBottomRightCurveClockwise)

        given(bottomLeftCurve).willReturn(someBottomLeftCurve)
        given(bottomLeftCurveClockWise).willReturn(someBottomLeftCurveClockWise)
    }

    val curvesForArrowProperties = CurvesForArrowProperties()

    @Test fun shouldCreateBottomCurvesArrayCorrectly() {
        // given
        val properties = ArrowProperties(Position.Bottom)
        val expected = arrayOf(
                someCurveOpen,
                someCurveClose,
                someBottomRightCurve,
                someTopRightCurve,
                someTopLeftCurve,
                someBottomLeftCurve
        )

        // when
         val actualArray = curvesForArrowProperties(properties.attachment, bubblePoints, bubbleArrow)

        // then
        Assert.assertArrayEquals(expected, actualArray)
    }

    @Test fun shouldCreateTopCurvesArrayCorrectly() {
        // given
        val properties = ArrowProperties(Position.Top)
        val expected = arrayOf(
                someCurveOpen,
                someCurveClose,
                someTopRightCurveClockwise,
                someBottomRightCurveClockwise,
                someBottomLeftCurveClockWise,
                someTopLeftCurveClockwise
        )

        // when
         val actualArray = curvesForArrowProperties(properties.attachment, bubblePoints, bubbleArrow)

        // then
        assertArrayEquals(expected, actualArray)
    }

    @Test fun shouldCreateLeftCurvesArrayCorrectly() {
        // given
        val properties = ArrowProperties(Position.Left)
        val expected = arrayOf(
                someCurveOpen,
                someCurveClose,
                someBottomLeftCurve,
                someBottomRightCurve,
                someTopRightCurve,
                someTopLeftCurve
        )

        // when
         val actualArray = curvesForArrowProperties(properties.attachment, bubblePoints, bubbleArrow)

        // then
        assertArrayEquals(expected, actualArray)
    }

    @Test fun shouldCreateRightCurvesArrayCorrectly() {
        // given
        val properties = ArrowProperties(Position.Right)
        val expected = arrayOf(
                someCurveOpen,
                someCurveClose,
                someBottomRightCurveClockwise,
                someBottomLeftCurveClockWise,
                someTopLeftCurveClockwise,
                someTopRightCurveClockwise
        )

        // when
        val actualArray = curvesForArrowProperties(properties.attachment, bubblePoints, bubbleArrow)

        // then
        assertArrayEquals(expected, actualArray)
    }
}