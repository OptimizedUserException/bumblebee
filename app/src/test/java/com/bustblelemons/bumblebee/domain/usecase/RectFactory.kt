package com.bustblelemons.bumblebee.domain.usecase

import android.graphics.Rect

object RectFactory {

    fun someRects() = someRects(10)
    fun someRects(count: Int): List<Rect> {
        return (0 until count).map {
            Rect(it, it, it + count, it + count)
        }
    }

}
