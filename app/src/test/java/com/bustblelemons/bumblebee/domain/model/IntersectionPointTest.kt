package com.bustblelemons.bumblebee.domain.model

import android.graphics.Point
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class IntersectionPointTest {

    @Test fun shouldCalculateXCorrectly() {
        // given
        val lineOne = Point(1, 0) to Point(1, 10)
        val lineTwo = Point(0, 1) to Point(10, 1)
        val expectedX = 1

        // when
        val actualX = IntersectionPoint(lineOne, lineTwo).x


        // then
        assertEquals(expectedX, actualX)
    }

    @Test fun shouldCalculateYCorrectly() {
        // given
        val lineOne = Point(1, 0) to Point(1, 10)
        val lineTwo = Point(0, 1) to Point(10, 1)
        val expectedY = 1

        // when
        val actualY = IntersectionPoint(lineOne, lineTwo).y


        // then
        assertEquals(expectedY, actualY)
    }

    @Test fun shouldCalculateXForAngledCorrectly() {
        // given
        val lineOne = Point(1, 1) to Point(10, 10)
        val lineTwo = Point(2, 0) to Point(2, 10)
        val expectedX = 2

        // when
        val actualX = IntersectionPoint(lineOne, lineTwo).x


        // then
        assertEquals(expectedX, actualX)
    }

    @Test fun shouldCalculateYForAngledCorrectly() {
        // given
        val lineOne = Point(1, 1) to Point(10, 10)
        val lineTwo = Point(2, 0) to Point(2, 10)
        val expectedY = 2

        // when
        val actualY = IntersectionPoint(lineOne, lineTwo).y


        // then
        assertEquals(expectedY, actualY)
    }

    @Test fun shouldCalculateXForNegativeAndAngledLinesSpace() {
        // given
        val lineOne = Point(1, 1) to Point(10, 10)
        val lineTwo = Point(1, -1) to Point(10, -1)
        val expectedX = -1

        // when
        val actualY = IntersectionPoint(lineOne, lineTwo).x


        // then
        assertEquals(expectedX, actualY)
    }

    @Test fun shouldCalculateYForNegativeAndAngledLinesSpace() {
        // given
        val lineOne = Point(1, 1) to Point(10, 10)
        val lineTwo = Point(1, -1) to Point(10, -1)
        val expectedY = -1

        // when
        val actualY = IntersectionPoint(lineOne, lineTwo).y


        // then
        assertEquals(expectedY, actualY)
    }
}