package com.bustblelemons.bumblebee.domain.model.functions

import android.graphics.Rect
import com.bustblelemons.bumblebee.ui.cameragrid.ThreeByThreeCameraGrid
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class ThreeByThreeFaceToBubbleRectsTest {

    lateinit var mapper: ThreeByThreeFaceToBubbleRects
    @Mock
    lateinit var cameraProvider: ThreeByThreeCameraGridProvider

    private val testCameraGrid = ThreeByThreeCameraGrid(Rect(0, 0, 3000, 3000))

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        mapper = ThreeByThreeFaceToBubbleRects(cameraProvider)
    }

    @Test
    fun shouldMapTopLeftGridCellCorrectly() {
        // given
        given(cameraProvider.invoke()).willReturn(testCameraGrid)
        val faceRect = testCameraGrid.center
        val expectecd = testCameraGrid.topLeft

        // when
        val bubbleRect = mapper.invoke(faceRect)

        // then
        assertEquals(expectecd, bubbleRect)
    }
}