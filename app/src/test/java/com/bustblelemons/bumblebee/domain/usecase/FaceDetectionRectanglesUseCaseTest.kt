package com.bustblelemons.bumblebee.domain.usecase

import com.bustblelemons.bumblebee.RectanglesFactory
import com.bustblelemons.bumblebee.anyObject
import com.bustblelemons.bumblebee.repository.facesdetection.FacesDetectedRepository
import io.reactivex.Flowable
import org.junit.Test
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock

class FaceDetectionRectanglesUseCaseTest {

    val testFaces = RectanglesFactory.someFaces()

    @Test fun shouldCreateStreamCorrectly() {
        // given
        val schedulers = TestRxSchedulers()
        val comparator = mockComparator()
        val repository = mockRepository()
        val usecase = FaceDetectionRectanglesUseCase(repository, schedulers, comparator)
        val expectedCount = FaceDetectionRectanglesUseCase.FACE_RECTANGLES_LIMIT

        // when
        val testObserver = usecase.rxStream.test()

        // then
        testObserver.assertComplete()
        testObserver.assertValueCount(expectedCount)

    }

    private fun mockRepository(): FacesDetectedRepository {
        return mock(FacesDetectedRepository::class.java).apply {
            given(this.detectFaces)
                    .willReturn(Flowable.fromArray(testFaces.toTypedArray()))
        }
    }

    private fun mockComparator(): RectangleComparator {
        return mock(RectangleComparator::class.java).apply {
            given(this.compare(anyObject(), anyObject())).willReturn(0)
        }
    }
}