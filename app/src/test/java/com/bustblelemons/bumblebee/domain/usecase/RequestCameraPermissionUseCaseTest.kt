package com.bustblelemons.bumblebee.domain.usecase

import com.bustblelemons.bumblebee.anyObject
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.Observable
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class RequestCameraPermissionUseCaseTest {

    @Test fun shouldRequestPermissionsCorrectly() {
        // given
        val rxPermissions = mock(RxPermissions::class.java)
        given(rxPermissions.request(anyObject())).willReturn(Observable.just(true))
        val usecase = RequestCameraPermissionUseCase(rxPermissions, TestRxSchedulers())

        // when
        val test = usecase.rxStream.test()

        // then
        test.assertComplete()
        test.assertValue(true)
    }
}