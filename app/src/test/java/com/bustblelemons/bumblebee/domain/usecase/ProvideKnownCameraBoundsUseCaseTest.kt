package com.bustblelemons.bumblebee.domain.usecase

import android.graphics.Rect
import com.bustblelemons.bumblebee.repository.camera.CameraRepository
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class ProvideKnownCameraBoundsUseCaseTest {

    private val schedulers: RxSchedulers = TestRxSchedulers()

    private lateinit var usecase: ProvideKnownCameraBoundsUseCase
    @Mock lateinit var repository: CameraRepository

    @Before fun setup() {
        MockitoAnnotations.initMocks(this)
        usecase = ProvideKnownCameraBoundsUseCase(schedulers, repository)
    }

    @Test fun shouldSetCameraBoundsViaBuilderCorrectly() {
        // given
        val someBounds = Rect()

        // when
        val useCaseWithBounds = usecase.cameraBounds(someBounds)

        // then
        assertNotNull(useCaseWithBounds)
    }

    @Test fun shouldCreateStreamCorrectly() {
        // given
        val someBounds = Rect()
        val useCaseWithBounds = usecase.cameraBounds(someBounds)

        // when
        val testObserver = useCaseWithBounds.rxStream.test()

        // then
        testObserver.assertComplete()
        testObserver.assertNoErrors()
    }

    @Test(expected = IllegalArgumentException::class)
    fun shouldThrowWhenBoundsNotSet() {
        // when
        val testObserver = usecase.rxScheduledStream.test()

        // then
        testObserver.assertTerminated()
        testObserver.assertError(java.lang.IllegalArgumentException::class.java)
    }
}