package com.bustblelemons.bumblebee.domain.usecase

import io.reactivex.Scheduler
import io.reactivex.schedulers.TestScheduler

class TestRxSchedulers : RxSchedulers {
    override val observeOn: Scheduler get() = TestScheduler()
    override val subscribeOn: Scheduler get() = TestScheduler()
}