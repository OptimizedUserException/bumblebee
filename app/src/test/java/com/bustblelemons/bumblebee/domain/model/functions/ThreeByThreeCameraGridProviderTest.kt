package com.bustblelemons.bumblebee.domain.model.functions

import android.graphics.Rect
import com.bustblelemons.bumblebee.repository.camera.CameraRepository
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class ThreeByThreeCameraGridProviderTest {

    @Test fun shouldProvideCameraGridCorrectly() {
        // given
        val repository = mock(CameraRepository::class.java)
        val expectedBounds = Rect(0, 0, 100, 200)
        given(repository.cameraBounds).willReturn(expectedBounds)
        val provider = ThreeByThreeCameraGridProvider(repository)

        // when
        val actualGrid = provider.invoke()

        // then
        assertEquals(actualGrid.cameraBounds, expectedBounds)
    }
}