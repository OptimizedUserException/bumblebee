package com.bustblelemons.bumblebee.domain.model.functions

import android.graphics.Rect
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class RectBySizeTest {

    lateinit var comparator: RectBySize

    private val smaller = Rect(10, 10, 10, 10)
    private val bigger = Rect ( 10, 10, 100, 100)

    @Before
    fun setup() {
        comparator = RectBySize()
    }

    @Test
    fun shouldReturnSmallerOneCorrectly() {
        // when
        val shouldBeMinusOne = comparator.compare(smaller, bigger)

        // then
        assertEquals(-1, shouldBeMinusOne)
    }

    @Test
    fun shouldReturnBiggerOneCorrectly() {
        // when
        val shouldBeOne = comparator.compare(bigger, smaller)

        // then
        assertEquals(1, shouldBeOne)
    }
}