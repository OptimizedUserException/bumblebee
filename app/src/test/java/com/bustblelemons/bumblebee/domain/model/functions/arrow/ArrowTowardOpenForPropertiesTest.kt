package com.bustblelemons.bumblebee.domain.model.functions.arrow

import android.graphics.Point
import com.bustblelemons.bumblebee.domain.model.ArrowProperties
import com.bustblelemons.bumblebee.domain.model.Position
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class ArrowTowardOpenForPropertiesTest {

    private val arrowTowardOpenForProperties = ArrowTowardOpenForProperties()

    @Test fun shouldCalculateForLeftAttachmentPointTop() {
        //given
        val start = Point(60, 120)
        val point = Point(20, 20)
        val properties = ArrowProperties(Position.Left, Position.Top)
        val expected = Point(40, 120)

        // when
        val actualPoint = arrowTowardOpenForProperties(properties, start, point)

        // then
        assertEquals(expected.x, actualPoint.x)
        assertEquals(expected.y, actualPoint.y)
    }

    @Test fun shouldCalculateForLeftAttachmentPointBottom() {
        //given
        val start = Point(60, 120)
        val point = Point(20, 20)
        val properties = ArrowProperties(Position.Left, Position.Bottom)
        val expected = Point(20, 120)

        // when
             val actualPoint = arrowTowardOpenForProperties(properties, start, point)

        // then
        assertEquals(expected.x, actualPoint.x)
        assertEquals(expected.y, actualPoint.y)
    }

    @Test fun shouldCalculateForTopAttachmentPointLeft() {
        //given
        val start = Point(50, 100)
        val point = Point(0, 0)
        val properties = ArrowProperties(Position.Top, Position.Left)
        val expected = Point(50, 50)

        // when
             val actualPoint = arrowTowardOpenForProperties(properties, start, point)

        // then
        assertEquals(expected.x, actualPoint.x)
        assertEquals(expected.y, actualPoint.y)
    }

    @Test fun shouldCalculateForTopAttachmentPointRight() {
        //given
        val start = Point(50, 100)
        val point = Point(100, 50)
        val properties = ArrowProperties(Position.Top, Position.Right)
        val expected = Point(50, 50)

        // when
             val actualPoint = arrowTowardOpenForProperties(properties, start, point)

        // then
        assertEquals(expected.x, actualPoint.x)
        assertEquals(expected.y, actualPoint.y)
    }

    @Test fun shouldCalculateForRightAttachmentPointTop() {
        //given
        val start = Point(50, 100)
        val point = Point(0, 200)
        val properties = ArrowProperties(Position.Bottom, Position.Left)
        val expected = Point(50, 150)

        // when
             val actualPoint = arrowTowardOpenForProperties(properties, start, point)

        // then
        assertEquals(expected.x, actualPoint.x)
        assertEquals(expected.y, actualPoint.y)
    }

    @Test fun shouldCalculateForRightAttachmentPointBottom() {
        //given
        val start = Point(50, 100)
        val point = Point(0, 200)
        val properties = ArrowProperties(Position.Bottom, Position.Left)
        val expected = Point(50, 150)

        // when
             val actualPoint = arrowTowardOpenForProperties(properties, start, point)

        // then
        assertEquals(expected.x, actualPoint.x)
        assertEquals(expected.y, actualPoint.y)
    }

    @Test fun shouldCalculateForBottomAttachmentPointLeft() {
        //given
        val start = Point(50, 100)
        val point = Point(0, 200)
        val properties = ArrowProperties(Position.Bottom, Position.Left)
        val expected = Point(50, 150)

        // when
             val actualPoint = arrowTowardOpenForProperties(properties, start, point)

        // then
        assertEquals(expected.x, actualPoint.x)
        assertEquals(expected.y, actualPoint.y)
    }

    @Test fun shouldCalculateForBottomAttachmentPointRight() {
        //given
        val start = Point(50, 100)
        val point = Point(200, 200)
        val properties = ArrowProperties(Position.Bottom, Position.Right)
        val expected = Point(start.x, point.y)

        // when
             val actualPoint = arrowTowardOpenForProperties(properties, start, point)

        // then
        assertEquals(expected.x, actualPoint.x)
        assertEquals(expected.y, actualPoint.y)
    }
}