package com.bustblelemons.bumblebee

import com.bustblelemons.bumblebee.domain.model.BubbleWithDirection
import org.mockito.Mockito.mock

object DirectedComicBubblesFactory {

    fun someBubbles(): List<BubbleWithDirection> {
        return (0..10).map { oneBubble }
    }

    val oneBubble: BubbleWithDirection
        // This is easier to mock
        get() = mock(BubbleWithDirection::class.java)
}